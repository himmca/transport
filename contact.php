<?php include('inc/header.php')?>
<?php include('inc/config.php')?>
<section class="breadcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h1>About Us</h1>
                    <ul class="breadcrumb_style_2">
                        <li><a href="index.php">Home</a><span class="ti-angle-double-right"
                                id="breadcumb_sperator_icon"></span></li>
                        <li>Contact Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact_map">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="tran_map"  data-aos="fade-up" data-aos-duration="1500">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14008.4893674217!2d77.36689594776574!3d28.626095187446758!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce5675b54f2d9%3A0xb7f406cd59d3ba7c!2sFedex%20Courier%20Ship%20Site%20Noida!5e0!3m2!1sen!2sin!4v1573905375826!5m2!1sen!2sin"
                        width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="contact_info_sec1">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact_info_title">
                    <h4 class="text_overlay"  data-aos="fade-up" data-aos-duration="1500">
                        Contact Us
                    </h4>
                    <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500"> Contact Us</h2>
                    <p class="how_desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt
                        ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact_info_sec1_box">
                    <div class="contat_info_icon">
                        <i class="flaticon-smartphone"></i>
                    </div>
                    <div class="icon_content">
                        <h4>Phone number</h4>
                        <h5><a href="tel:+91 564 548 4854">+91 564 548 4854</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact_info_sec1_box text_shadow">
                    <div class="contat_info_icon">
                        <i class="flaticon-email"></i>
                    </div>
                    <div class="icon_content">
                        <h4>Email address</h4>
                        <h5><a href="mailto:thewebmaxinfo@gmail.com">thewebmaxinfo@gmail.com</a></h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="contact_info_sec1_box">
                    <div class="contat_info_icon">
                        <i class="flaticon-placeholder"></i>
                    </div>
                    <div class="icon_content">
                        <h4>Address info</h4>
                        <h5>340-G, Kala Pathar Rd, Makanpur, Nyay Khand 2, Indirapuram, Ghaziabad, Uttar Pradesh 201010
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="contact_form_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="contact_info_title">
                    <h4 class="text_overlay"  data-aos="fade-up" data-aos-duration="1500">
                        Get In Touch
                    </h4>
                    <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500"> Get In Touch</h2>
                </div>
            </div>
        </div>
        <form class="contact_form cons_contact_form" method="post" action="form-handler.php">
            <div class="contact_one">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form_group_crt">
                            <input name="username" type="text" required="" placeholder="Name">
                            <span class="spin"></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form_group_crt">
                            <input name="email" type="text" required="" placeholder="Email">
                            <span class="spin"></span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form_group_crt">
                            <textarea name="message" rows="4" required="" placeholder="Message"></textarea>
                            <span class="spin"></span>
                        </div>
                    </div>
                    <div class="text_left col-md-12">
                        <div class="contact_btn_submit">
                            <button name="submit" type="submit" value="Submit" class="site_button site_btn_effect hover_btn_effect">
                                   Submit   <span class="ti-arrow-right" id="track_icon"></span> 
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

<?php include('inc/footer.php')?>