
$(document).ready(function () {
  var owl = $('.owl-carousel');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    animateOut: 'fadeOut',
    animateIn: 'fadeInLeft',
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[1500])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})
});


$('.testimonial_home').owlCarousel({
  loop: true,
  stagePadding: 0,
  margin: 0,
  autoplay:true,
  autoplayTimeout: 4000,
  autoplayHoverPause: true,
  responsiveClass: true,
 
  responsive: {
    0: {
      items: 1,
      nav: true
    },
    600: {
      items: 1,
      nav: true
    },
    1000: {
      items: 2,
      nav: true,
      loop: false
    }
  }
})


$(document).ready(function(){

    $('.mobo_button').click(function(){

        $('.nav_right').toggleClass('active__menu');

    });
    AOS.init({
        duration : 1500,
      })
});


$('.start_number').each(function () {
  var size = $(this).text().split(".")[1] ? $(this).text().split(".")[1].length : 0;
  $(this).prop('Counter', 0).animate({
     Counter: $(this).text()
  }, {
     duration: 5000,
     step: function (func) {
        $(this).text(parseFloat(func).toFixed(size));
     }
  });
});



$(document).ready(function(){
    $(window).scroll(function (){

      if($(window).scrollTop() >=300){
        $('.main_header').addClass('fixed_header')
      }
      else{
        $('.main_header').removeClass('fixed_header')
      }

    });
  });

  $(document).ready(function(){
    $('.mobo_button').click(function(){
        $(this).toggleClass("open")
        $(".mobo__menu_link").slideToggle(500);
    });
});

$(document).ready(function(){
  $(window).scroll(function (){

    if($(window).scrollTop() >=300){
      $('.mobile_menu').addClass('fixed_mobo_header')
    }
    else{
      $('.mobile_menu').removeClass('fixed_mobo_header')
    }

  });
});
