<?php include('inc/header.php')?>
<?php include('inc/config.php')?>
<section class="breadcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h1>About Us</h1>
                    <ul class="breadcrumb_style_2">
                        <li><a href="index.php">Home</a><span class="ti-angle-double-right"
                                id="breadcumb_sperator_icon"></span></li>
                        <li>Service</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="service_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500" >
                    our service
                </h4>
                <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500">Our Service</h2>
                <p class="service_desc"  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
            </div>
            <div class="col-md-3">
                <div class="service_box_wrap">
                    <div class="service_icon"  data-aos="fade-up" data-aos-duration="1500">
                        <i class="flaticon-truck"></i>
                    </div>
                    <div class="service_box_content">
                        <h4  data-aos="fade-up" data-aos-duration="1500">Land Transport</h4>
                        <p  data-aos="fade-up" data-aos-duration="1500"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur
                            labore
                            dolore</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service_box_wrap">
                    <div class="service_icon"  data-aos="fade-up" data-aos-duration="1500">
                        <i class="flaticon-data-warehouse"></i>
                    </div>
                    <div class="service_box_content">
                        <h4  data-aos="fade-up" data-aos-duration="1500">Warehousing</h4>
                        <p  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur
                            labore
                            dolore</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service_box_wrap">
                    <div class="service_icon"  data-aos="fade-up" data-aos-duration="1500">
                        <i class="flaticon-box-truck"></i>
                    </div>
                    <div class="service_box_content">
                        <h4  data-aos="fade-up" data-aos-duration="1500">Door to Door Service</h4>
                        <p  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur
                            labore
                            dolore</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="service_box_wrap">
                    <div class="service_icon"  data-aos="fade-up" data-aos-duration="1500">
                        <i class="flaticon-crane"></i>
                    </div>
                    <div class="service_box_content">
                        <h4  data-aos="fade-up" data-aos-duration="1500">Logistic Solustions</h4>
                        <p  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur
                            labore
                            dolore</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="show_btn">
                    <a href="contact.php" class="hover_btn_effect"  data-aos="fade-up" data-aos-duration="1500">Get In Touch<span class="ti-arrow-right"></span></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="goodness">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="goodness_title">
                    <h4 class="text_overlay"  data-aos="fade-up" data-aos-duration="1500">
                        Why Choose Us
                    </h4>
                    <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500"> Why Choose Us</h2>
                    <p class="how_desc"  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="goodness_bg"  data-aos="fade-right" data-aos-duration="1500">
                    <img src="assets/img/courier-man.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="goodness_box"  data-aos="fade-up" data-aos-duration="1500">
                            <div class="media">
                                <div class="goodness_icon">
                                    <i class="flaticon-package-for-delivery"></i>
                                </div>
                                <div class="goodness_content media-body">
                                    <span>Packaging and Storage</span>
                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                                        Curae. Praesent
                                        pellentesque diam vitae nibh aliquam faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="goodness_box"  data-aos="fade-up" data-aos-duration="1500">
                            <div class="media">
                                <div class="goodness_icon">
                                    <i class="flaticon-shield-1"></i>
                                </div>
                                <div class="goodness_content media-body">
                                    <span>Safety & Quality</span>
                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                                        Curae. Praesent
                                        pellentesque diam vitae nibh aliquam faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="goodness_box"  data-aos="fade-up" data-aos-duration="1500">
                            <div class="media">
                                <div class="goodness_icon">
                                    <i class="flaticon-tree-with-many-leaves"></i>
                                </div>
                                <div class="goodness_content media-body">
                                    <span>Care for Environment</span>
                                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                                        Curae. Praesent
                                        pellentesque diam vitae nibh aliquam faucibus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="track_order">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text_overlay"  data-aos="fade-up" data-aos-duration="1500">
                    Track Order
                </h4>
                <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500"> Track Order</h2>
                <p class="how_desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
            </div>
            <div class="col-md-6">
                <div class="track_order_img"  data-aos="fade-right" data-aos-duration="1500">
                    <img src="assets/img/forklift_Image.png" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="track_order_wrap"  data-aos="fade-up" data-aos-duration="1500">
                    <form action="#">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="input_group">
                                    <input type="text" class="form_contro_wrap" placeholder="Name" name="name"
                                        id="name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input_group">
                                    <input type="number" class="form_contro_wrap" placeholder="phone No."
                                        name="phone No." id="phone No.">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="input_group">
                                    <input type="text" class="form_contro_wrap" placeholder="Enter traking id">
                                </div>
                            </div>
                            <div class="col-md-12">
                              <div class="text_center">
                              <button type="submit" class="chek_order_btn hover_btn_effect">Check Now<span class="ti-arrow-right" id="track_icon"></span></button>
                              </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="top__foot">
        <div class="container custom__container">
            <div class="row align-items-center">
                <div class="col-md-5">
                    <div class="news__letter_content">
                        <h6>Subscribe Our Newsletter</h6>
                        <p>Luisque dapibus lacus non pulvinar lobo.</p>
                    </div>
                </div>
                <div class="col-md-7">
                    <form action="#">
                        <div class="news_for">
                            <input class="form__control_news" placeholder="Enter your email" type="email">
                            <button type="submit" class="btn newsletter__btn_sumbit blob-small">Subscribe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include('inc/footer.php')?>