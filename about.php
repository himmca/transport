<?php include('inc/header.php')?>
<?php include('inc/config.php')?>
<section class="breadcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h1>About Us</h1>
                    <ul class="breadcrumb_style_2">
                        <li><a href="index.php">Home</a><span class="ti-angle-double-right"
                                id="breadcumb_sperator_icon"></span></li>
                        <li>About Us</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_company" id="none_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="about_company_bg" data-aos="fade-up" data-aos-duration="1500">
                    <img src="assets/img/pic4.jpg" alt="">
                </div>
            </div>
            <div class="col-md-7">
                <div class="about_content_wrap">
                    <div class="about_yers_sec" data-aos="fade-up" data-aos-duration="1500">
                        <span class="years_number">25</span>
                        <span class="years_content_large">Years of Experience</span>
                    </div>
                    <div class="about_bottom_content">
                        <h4 data-aos="fade-up" data-aos-duration="1500">Improving quality of life with an integrated
                            unified approach</h4>
                        <p data-aos="fade-up" data-aos-duration="1500">Dummy text is also used to demonstrate the
                            appearance of different typefaces and layouts, and
                            in
                            general the content of dummy text is nonsensical. Due to its widespread use texts. Ut wisi
                            enim ad minim
                            veniam, quis nostrud exerci tation.</p>
                        <p data-aos="fade-up" data-aos-duration="1500">Dummy text is also used to demonstrate the
                            appearance of different typefaces and layouts, and
                            in
                            general the content of dummy text is nonsensical. Due to its widespread use texts. Ut wisi
                            enim ad minim
                            veniam, quis nostrud exerci tation.</p>
                    </div>
                    <a href="contact.php" class="more__about_btn hover_btn_effect  data-aos=" fade-up"
                        data-aos-duration="1500"">Get In Touch</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class=" counter_sec">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="counter_sec_box">
                                        <div class="counet_icon" data-aos="fade-up" data-aos-duration="1500">
                                            <i class="flaticon-umbrella"></i>
                                        </div>
                                        <div class="counter_content" data-aos="fade-up" data-aos-duration="1500">
                                            <span class="start_number">2340</span>
                                            <p>TONNES TRANSPORTED</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="counter_sec_box">
                                        <div class="counet_icon" data-aos="fade-up" data-aos-duration="1500">
                                            <i class="flaticon-user"></i>
                                        </div>
                                        <div class="counter_content" data-aos="fade-up" data-aos-duration="1500">
                                            <span class="start_number">230</span>
                                            <p>COMPANY EMPLOYEES</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="counter_sec_box">
                                        <div class="counet_icon" data-aos="fade-up" data-aos-duration="1500">
                                            <i class="flaticon-delivery-truck"></i>
                                        </div>
                                        <div class="counter_content" data-aos="fade-up" data-aos-duration="1500">
                                            <span class="start_number">6500</span>
                                            <p>Delivered Packages</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="counter_sec_box">
                                        <div class="counet_icon" data-aos="fade-up" data-aos-duration="1500">
                                            <i class="flaticon-donation"></i>
                                        </div>
                                        <div class="counter_content" data-aos="fade-up" data-aos-duration="1500">
                                            <span class="start_number">1900</span>
                                            <p>SATISFIED CLIENTS</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
</section>

<section class="mision_vision">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500">
                    our Promise
                </h4>
                <h2 class="servicee_title" data-aos="fade-up" data-aos-duration="1500">Our Promise</h2>
            </div>
            <div class="col-md-6">
                <div class="mision_box" data-aos="fade-up" data-aos-duration="1500">
                    <h3>Our Goal</h3>
                    <p>
                        Our purpose is to empower human capital through industry accredited, nationally and
                        internationally recognized courses to close the skill gap. We build value for professionals
                        through our top trainers, current and on-demand content, certification through accredited
                        bodies, localised delivery and a robust technology platform.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="mision_box" data-aos="fade-up" data-aos-duration="1500">
                    <h3>Our Aspirations</h3>
                    <p>
                        Our purpose is to empower human capital through industry accredited, nationally and
                        internationally recognized courses to close the skill gap. We build value for professionals
                        through our top trainers, current and on-demand content, certification through accredited
                        bodies, localised delivery and a robust technology platform.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500">
                    Testimonials
                </h4>
                <h2 class="servicee_title" data-aos="fade-up" data-aos-duration="1500">Testimonials</h2>
                <p class="how_desc" data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, sed do eiusmod tempor
                    incididunt
                    ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
            </div>
        </div>
        <div class="owl-carousel owl-theme testimonial_home">
            <div class="item">
                <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_1.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_3.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_1.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_3.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="top__foot">
    <div class="container custom__container">
        <div class="row align-items-center">
            <div class="col-md-5">
                <div class="news__letter_content">
                    <h6>Subscribe Our Newsletter</h6>
                    <p>Luisque dapibus lacus non pulvinar lobo.</p>
                </div>
            </div>
            <div class="col-md-7">
                <form action="#">
                    <div class="news_for">
                        <input class="form__control_news" placeholder="Enter your email" type="email">
                        <button type="submit" class="btn newsletter__btn_sumbit blob-small">Subscribe</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<?php include('inc/footer.php')?>