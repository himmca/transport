<?php
class CommonClass{
	function db(){
		return $conn=mysqli_connect("localhost","root","","transport");		
		
	}
	function slugstring($string){
		return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
	}
	/*------------------------------------------- Generate Slug ----------------------------------------------------*/
	function Slug($string){
			return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
	}
	function check_slug_exists($slug,$table_name,$field_name,$id){
		$cond=($id!='')?" AND id!='".$id."'":"";		
		$qry=mysqli_query($this->db(),"SELECT * FROM ".$table_name." WHERE ".$field_name."='$slug' ".$cond."");
		if(mysqli_num_rows($qry))
			return true;		
		else		
			return false;					
	}
	function create_unique_slug($val,$table_name,$field_name,$id,$counter=0)	{		
		if($counter>0)		
			$slug = $this->Slug($val).'-'.$counter;		
		else		
			$slug = $this->Slug($val);	
		$check_slug_exists =$this->check_slug_exists($slug,$table_name,$field_name,$id);
		if($check_slug_exists){
			$counter++;
			 return $this->create_unique_slug($val,$table_name,$field_name,$id, $counter);			
		}
		return $slug;		
	}
	function SendEmailFun($to,$subject,$message){
		$headers = "From: PtAcademy <".ADMIN_EMAIL.">\r\n";
		$headers .= "Reply-To: <".ADMIN_EMAIL.">\r\n";  
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		if(mail($to, $subject, $message, $headers))
		   return true;
		else
		 return false;
	}
	function GenerateUsername($username,$counter=0){
		if($counter>0)		
			$newusername = $username.$counter;		
		else		
			$newusername = $username;	
		$check_slug_exists =$this->check_slug_exists($newusername,'user_details','username','');
		if($check_slug_exists){
			$counter++;
			 return $this->GenerateUsername($username,$counter);			
		}
		return $newusername;
	}
	function GenerateRandomPassword($len) {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < $len; $i++) {
	  		$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	function cleanString($string) {
		//$detagged = strip_tags($string);
		$detagged = $string;
	    if(get_magic_quotes_gpc()) {
	        $stripped = stripslashes($detagged);
	        $escaped = mysqli_real_escape_string($this->db(),$stripped);
	    } else 
	        $escaped = mysqli_real_escape_string($this->db(),$detagged);	    
	    return $escaped;
	}
	function sanitize($input) {
	    if (is_array($input)) {
	        foreach($input as $var=>$val) {
	            $output[$var] =$this->sanitize($val);
	        }
	    }
	    else {
	        $input  = $this->cleanString($input);
	        $output = $input;
	    }
	    return $output;
	}
	function InsertedWithId($qry){		
		$inserted=mysqli_query($this->db(),$qry);
		return $insertedid=mysqli_insert_id($this->db());
		return ($inserted)?$insertedid:false;
	}
	function NormalQuery($qry){		
		$qry=mysqli_query($this->db(),$qry);
		return ($qry)?true:false;
	}
	function ResultWithArrayData($qry){		
		$qry=mysqli_query($this->db(),$qry);
		if(mysqli_num_rows($qry)){
			$datalist=array();
			while($row=mysqli_fetch_assoc($qry)){
				$datalist[]=$row;
			}
			return $datalist;
		}
		else
			return false;

	}
	function ResultWithSingleRow($qry){
		$qry=mysqli_query($this->db(),$qry);
		return mysqli_fetch_assoc($qry);
	}
	function CheckDataExist($qry){
		$qry=mysqli_query($this->db(),$qry);
		if(mysqli_num_rows($qry))
			return mysqli_fetch_assoc($qry);
		else
			return false;
	}
	function CountRecords($qry){
		$qrydata=mysqli_query($this->db(),$qry);
		$row=mysqli_fetch_assoc($qrydata);
		return $row['nums'];
	}

	function compress_image($source_url, $destination_url, $quality){
		   $info = getimagesize($source_url); if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url); elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url); elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url); imagejpeg($image, $destination_url, $quality); return $destination_url;
	}
	function set_thumb($file, $photos_dir,$thumbs_dir, $square_size,$height_t, $quality) {       
        if (!file_exists($thumbs_dir."/".$file)) {             
                list($width, $height, $type, $attr) = getimagesize($photos_dir."/".$file);
                /*if($width> $height) {
                        $width_t=$square_size;                       
                        $height_t=round($height/$width*$square_size);                      
                        $off_y=ceil(($width_t-$height_t)/2);
                        $off_x=0;
                } elseif($height> $width) {
                        $height_t=$square_size;
                        $width_t=round($width/$height*$square_size);
                        $off_x=ceil(($height_t-$width_t)/2);
                        $off_y=0;
                }
                else {
                        $width_t=$height_t=$square_size;
                        $off_x=$off_y=0;
                }*/
                $thumb_p = imagecreatetruecolor($square_size, $height_t);
				$thumb=imagecreatefromjpeg($photos_dir."/".$file);                
                imagecopyresampled($thumb_p, $thumb, 0,0, 0, 0, $square_size, $height_t, $width, $height);
                imagejpeg($thumb_p,$thumbs_dir."/".$file,$quality);
                }
	} 
	function getCityNameUsingIp($ip_address){
		$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip_address));
		if($query && $query['status'] == 'success') {
		  return $query['city'];
		} else 
		  return false;
	}
	 function CountWord($lenght,$data){
	 	return implode(' ', array_slice(explode(' ', strip_tags(stripslashes($data))), 0, $lenght));
		// preg_match("/(?:\w+(?:\W+|$)){0,$lenght}/", $data, $matches);
          //return $matches[0];
	}
	/***************************************************** Bind Category ***********************************************************************************/
	function MainCategoryDropDown($catid){
		$qry=mysqli_query($this->db(),"SELECT * FROM category WHERE status='1'");
		if(mysqli_num_rows($qry)){
			$dropdown='<option value="">----- SELECT --------</option>';
			while($row=mysqli_fetch_assoc($qry)){
				$selected=($row['id']==$catid)?"selected":"";
				$dropdown.='<option value="'.$row['id'].'" '.$selected.'>'.$row['cat_name'].'</option>';
			}
			return $dropdown;
		}
		else
			return '<option value="">----- SELECT --------</option>';
	}
	function MainBodyPart($catid){
		$qry=mysqli_query($this->db(),"SELECT * FROM body_part WHERE status='1' AND parent_id='0'");
		if(mysqli_num_rows($qry)){
			$dropdown='<option value="">----- SELECT --------</option>';
			while($row=mysqli_fetch_assoc($qry)){
				$selected=($row['id']==$catid)?"selected":"";
				$dropdown.='<option value="'.$row['id'].'" '.$selected.'>'.$row['part_name'].'</option>';
			}
			return $dropdown;
		}
		else
			return '<option value="">----- SELECT --------</option>';
	}
	function BindCategoryWithCheckbox($qry,$catid,$name){
		$qry=mysqli_query($this->db(),$qry);
		if(mysqli_num_rows($qry)){
			$checkbox='';
			while ($row=mysqli_fetch_assoc($qry)) {
				$checked="";
				if(in_array($row['id'], $catid))
					$checked="checked";
				$checkbox.='<div class="col-md-3"><label class="check"><div class="icheckbox_minimal-grey" ><input type="checkbox" class="'.$name.'" name="'.$name.'[]" '.$checked.' value="'.$row['id'].'"></div>'.$row['catname'].'</label></div>';
			}
			return $checkbox;
		}
	}
	function BindDropDown($qry,$catid){
		$qry=mysqli_query($this->db(),$qry);
		$checkbox='<option value="">------------- Select --------------------</option>';
		if(mysqli_num_rows($qry)){
			while ($row=mysqli_fetch_assoc($qry)) {
				$checked="";
				if($row['id']==$catid)
					$checked="selected";
				$checkbox.='<option value="'.$row['id'].'" '.$checked.'>'.$row['catname'].'</option>';
			}			
		}
		return $checkbox;
	}
	function BindCategoryWithCheckboxFront($qry,$catid,$name,$fun){
		$qry=mysqli_query($this->db(),$qry);
		if(mysqli_num_rows($qry)){
			$checkbox='';
			while ($row=mysqli_fetch_assoc($qry)) {
				$checked="";
				if(in_array($row['id'], $catid))
					$checked="checked";
				$checkbox.='<li><input type="checkbox" name="'.$name.'[]" value="'.$row['id'].'" class="check '.$name.'" '.$checked.' onchange=\''.$fun.'(this,"")\'>'.$row['catname'].'</li>';
			}
			return $checkbox;
		}
	}
	function removeHyperlink($string){
		$pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
		$replacement = "";
		return preg_replace($pattern, $replacement, $string);
	}
	function GenerateCapetcha($len){
		$alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < $len; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode(' ',$pass);
	}
	function LoginCredentialsEmail($data){
		$message="Hello ".$data['fname']." ".$data['lname']."<br><br>";
		$message.="Your Login Credential are following <br><br>";
		$message.="Username :".$data['username']."<br>";
		$message.="Password: ".$data['password']." <br><br>";
		$message.="Thanking You <br><br>";
		$message.="PtAcademy <br><br>";
		$this->SendEmailFun($data['email'],"Login Credential -PtAcademy",$message);
	}
}
?>