<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Transport</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="assets/vendors/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="assets/css/flaticon.css">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/custom.css">
  <link rel="stylesheet" type="text/css" media="screen" href="assets/css/media.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
  <link rel="stylesheet" href="assets/css/docs.theme.min.css">
  <link rel="stylesheet" href="assets/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/owlcarousel/assets/owl.theme.default.min.css">
  <link rel="stylesheet" href="assets/themify-icons/themify-icons.css">
  <link
    href="https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
  <script src="assets/owlcarousel/owl.carousel.js"></script>
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <script defer src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js"></script>
</head>
<body>
<header class="site_header">
    <div class="top_header">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="mt_topbar_left clearfix">
              <ul class="list_top_header">
                <li> <i class="fas fa-envelope"></i><a href="#"> automobilesdadri@gmail.com</a></li>
                <li> <i class="fas fa-phone"></i><a href="#"> 1800 121 910 000</a></li>
                <li> <i class="far fa-clock"></i><a href="#"> 24*7 OPEN </a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4">
            <div class="mt_topbar_right">
              <div class="appon_btn">
                <a href="contact.php" class="make_btn_effect">Make an Appointment</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="main_header">
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="logo_sec">
             <a href="index.php"><img src="assets/img/logo.png" alt=""></a>
            </div>
          </div>
          <div class="col-md-9">
            <div class="main_header_menu">
              <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="service.php">Services</a></li>
                <li><a href="testimonial.php">Testimonial</a></li>
                <!-- <li><a href="#">Career</a></li> -->
                <li><a href="contact.php">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>


  <section class="mobile_menu">
        <div class="mobile_menu_logo">
        <a href="index.php"><img src="assets/img/logo.jpg" alt=""></a>
        </div>
        <div class="mobo_button">
            <span class="one"></span>
            <span class="two"></span>
            <span class="three"></span>
        </div>
        <div class="mobo__menu_link " id="overlay">
            <ul>
            <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About Us</a></li>
                <li><a href="service.php">Services</a></li>
                <li><a href="testimonial.php">Testimonial</a></li>
                <!-- <li><a href="#">Career</a></li> -->
                <li><a href="contact.php">Contact Us</a></li>
            </ul>
        </div>
    </section>