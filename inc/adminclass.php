<?php
class AdminClass{
	var $CommonClass1;
	function AdminClass(){
		$this->CommonClass1=new CommonClass();
	}
	// admin change status of records
	function change_status($data){
		$tab=$data['tab'];
		if($tab=='sliders')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='director')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='achievers')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='testimonial')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='principal')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='popup')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='notice')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='gallery_images')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='gallery_detail')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='downloadpros')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='video_detail')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='newss')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='hot_news')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='calender')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='video')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='download')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='registration')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='chairman')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='news')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='newss')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='admission_form')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='tc_form')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='about')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='registration')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='career')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='jobs')
			$result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='faculty')
		    $result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		if($tab=='media')
		    $result=$this->CommonClass1->NormalQuery("UPDATE ".$tab." SET status=".$data['status']." WHERE id =".$data['id']." ");
		return ($result)?true:false;
	}
	// admin delete of records
function delete_record($data){
		$tab=$data['tab'];
		if($tab=='sliders')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='director')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='achievers')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='testimonial')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='principal')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='notice')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='hot_news')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='popup')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='newss')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='download')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='downloadpros')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='chairman')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='calender')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='news')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='gallery_images')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='gallery_detail')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='video_detail')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='video')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='admission_form')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='registration')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='about')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='tc_form')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='career')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='faculty')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='jobs')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab."  WHERE id =".$data['id']." ");
		if($tab=='contact_form')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		if($tab=='media')
		$result=$this->CommonClass1->NormalQuery("DELETE FROM ".$tab." WHERE id =".$data['id']." ");
		return $result;
	}
	function adminLoginCheck($data){
	    
	$count=$this->CommonClass1->CountRecords("SELECT count(*) as nums FROM admin WHERE email='".$data['email']."' AND password='".md5($data['password'])."'");
	return $count;
	}
	function addSlider($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image'])?",slider_image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `sliders` SET `order_no`=".$data['order']." ".$image." WHERE id=".$data['hiddenval']."");
		}
		else
			return $this->CommonClass1->NormalQuery("INSERT INTO `sliders`(`slider_image`, `order_no`,`created_date`) VALUES ('".$data['image']."','".$data['order']."',now())");
	}
function GetSliderList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM sliders  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td><img width=200px; src='uploads/slider/".$category['slider_image']."'></td><td>".$category['order_no']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a href='add-slider.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'sliders',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'sliders',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}


	
	function addVideoData($data){
		if($data['hiddenval']!=''){
			return $this->CommonClass1->NormalQuery("UPDATE `video` SET `url`='".$data['url']."'WHERE id=".$data['hiddenval']."");
		}
		else
		return $this->CommonClass1->NormalQuery("INSERT INTO `video`(`url`) VALUES ('".$data['url']."')");
	}
	function GetVideoList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM video  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['url']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-videos.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'video',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'video',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function addAboutData($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image1'])?",image1='".$data['image1']."'":'';
			$image1=!empty($data['image2'])?",image2='".$data['image2']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `about` SET `description`='".$data['description']."' ".$image." ".$image1." ");		}
		else
		
		return $this->CommonClass1->NormalQuery("INSERT INTO `about`(`description`,`image1`,`image2`) VALUES ('".$data['description']."','".$data['image1']."','".$data['image2']."')");
	}
	function GetAboutList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM about  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td><img width=200px; src='uploads/about/".$category['image1']."'></td><td><img width=200px; src='uploads/about/".$category['image2']."'></td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-about.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'about',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'about',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
	
	

	function addGalleryData($data){
		//if files have to update other go to else statement
		if($data['id']!=''){
			$sql = "Select * from gallery_images where id = " . $data['id'];
			$tempData =  $this->CommonClass1->ResultWithArrayData($sql);

			if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){	
				$countfiles = count($_FILES['image']['name']);
				$temp = '';
				for($i=0;$i<$countfiles;$i++){
					$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name'][$i]);
					move_uploaded_file($_FILES['image']['tmp_name'][$i],'uploads/gallery/'.$uploadpic);
					$temp = $temp . $uploadpic . '#';
				}
				$tempImageName = $tempData[0]['image'] . $temp;
				$sql = "UPDATE `gallery_images` SET image='".$tempImageName."' WHERE id=".$data['id']."";
				$status =  $this->CommonClass1->NormalQuery($sql);
				return $status;
			}else{
				 ;
			}

			$image=!empty($data['image'])?"image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `gallery_images` SET id=".$data['update']."  ".$image." WHERE id=".$data['update']."");
		}
		else{
			if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){	
				$countfiles = count($_FILES['image']['name']);
				$temp = '';
				for($i=0;$i<$countfiles;$i++){
					$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name'][$i]);
					move_uploaded_file($_FILES['image']['tmp_name'][$i],'uploads/gallery/'.$uploadpic);
					$temp = $temp . $uploadpic . '#';
				}
				$data['image']=$temp;
			} 
			return $this->CommonClass1->NormalQuery("INSERT INTO `gallery_images`(`image`,`created_date`) VALUES ('".$data['image']."',now())");
		}
	}

	function GetGalleryList(){
		
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM gallery_images  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td><img width=200px; src='uploads/gallery/".$category['image']."'><td>".$category['status']."</td>
					<td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a href='add-images.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'gallery_images',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'gallery_images',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function updateGalleryImages($data){
		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			
			if(!empty($data['update'])){
		
				foreach ($_FILES['image']['name'] as $k => $value){	
			
				$fotoload = date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
			
			move_uploaded_file($_FILES['image']['tmp_name'][$k], "uploads/gallery/".$fotoload);
			
			$fotoload =$CommonClass->NormalQuery("UPDATE `gallery_images` SET image = '".$fotoload."' WHERE id = $id");
					
		}
	
		}
	
		}
				 
		}
		
	
	function addPrincipalData($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `principal` SET `heading`='".$data['heading']."',`short_message`='".$data['short_message']."',`long_message`='".$data['long_message']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `principal`(`heading`,`short_message`,`long_message`,`image`) VALUES ('".$data['heading']."','".$data['short_message']."','".$data['long_message']."','".$data['image']."') ");
	}
	function GetPrincipalList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM principal  WHERE status != 2 ORDER BY id DESC ";;
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['heading']."</td><td>".$this->CommonClass1->CountWord(7,$category['short_message'])."</td><td><img src='uploads/principal/".$category['image']."'></td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-principal.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'principal',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'principal',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function addChairmanData($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `chairman` SET `heading`='".$data['heading']."',`message`='".$data['message']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `chairman`(`heading`,`message`,`image`) VALUES ('".$data['heading']."','".$data['message']."','".$data['image']."') ");
	}
	function GetChairmanList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM chairman  WHERE status != 2 ORDER BY id DESC ";;
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['heading']."</td><td>".$this->CommonClass1->CountWord(7,$category['message'])."</td><td><img src='uploads/chairman/".$category['image']."'></td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-chairman.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'chairman',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'chairman',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function addAchieversData($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `achievers` SET `topper_name`='".$data['topper_name']."',`name`='".$data['name']."',`percentage`='".$data['percentage']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `achievers`(`topper_name`,`name`,`percentage`,`image`) VALUES ('".$data['topper_name']."','".$data['name']."','".$data['percentage']."','".$data['image']."') ");
	}
	function GetAchieversList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM achievers  WHERE status != 2 ORDER BY id DESC ";;
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['name']."</td><td>".$category['topper_name']."</td><td><img src='uploads/achievers/".$category['image']."'></td><td>".$category['percentage']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-achievers.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'achievers',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'achievers',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
		function addTestimonialData($data){
		$result='';
		if(!empty($data['hiddenval'])){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			$result= $this->CommonClass1->NormalQuery("UPDATE `testimonial` SET `name`='".$data['name']."', `message`='".$data['message']."',`designation`='".$data['designation']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else{
			$result= $this->CommonClass1->NormalQuery("INSERT INTO `testimonial`(`name`, `message`,`designation`,`image`) VALUES ('".$data['name']."','".$data['message']."','".$data['designation']."','".$data['image']."') ");
		}
		return ($result)?true:false;
	}
	function GetTestimonialList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM testimonial  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td><img src='uploads/testimonial/".$category['image']."'></td><td>".$this->CommonClass1->CountWord(7,$category['message'])."</td><td>".$category['designation']."</td><td>".$category['name']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-testimonial.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'testimonial',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'testimonial',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function GetContactList(){
		$categoryData='';
						$query	="SELECT * FROM contact_form ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['student_name']."</td><td>".$category['parent_name']."</td><td>".$category['class']."</td><td>".$category['email']."</td><td>".$category['phone']."</td><td>".$category['message']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><button onclick=\"delete_row(this,'contact_form',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
	function GetAddmissionList(){
		$categoryData='';
						$query	="SELECT * FROM admission_form";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['student_name']."</td><td>".$category['admission_for']."</td><td>".$category['dob']."</td><td>".$category['nationality']."</td><td>".$category['mother_tongue']."</td><td>".$category['blood_group']."</td><td>".$category['gender']."</td><td>".$category['email']."</td><td>".$category['mobile_number']."</td><td>".$category['father_name']."</td><td>".$category['passport']."</td><td>".$category['mother_name']."</td><td>".$category['education']."</td><td>".$category['current_academic_year_to']."</td><td>".$category['current_academic_year_from']."</td><td>".$category['current_school']."</td><td>".$category['academic_year']."</td><td>".$category['grade']."</td><td>".$category['hear_about']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><button onclick=\"delete_row(this,'admission_form',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
	function GetCarrerList(){
		$categoryData='';
						$query	="SELECT * FROM career ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['applying_for']."</td><td>".$category['first_name']."</td><td>".$category['last_name']."</td><td>".$category['dob']."</td><td>".$category['gender']."</td><td>".$category['religion']."</td><td>".$category['email']."</td><td>".$category['mobile_number']."</td><td>".$category['nationality']."</td><td>".$category['marital_status']."</td><td>".$category['address']."</td><td>".$category['city']."</td><td>".$category['state']."</td><td>".$category['educational_qualication']."</td><td>".$category['year_of_passing']."</td><td>".$category['education_medium']."</td><td>".$category['training_qualification']."</td><td>".$category['specialisation']."</td><td>".$category['experience']."</td><td>".$category['current_salary']."</td><td>".$category['last_employer']."</td><td>".$category['expected_salary']."</td><td>".$category['designation']."</td><td>".$category['before_joining']."</td><td>".$category['message']."</td><td><a href='uploads/resume/".$category['resume']."' target='_blank'>Download Resume</a></td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><button onclick=\"delete_row(this,'career',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function addJobData($data){
		if($data['hiddenval']!=''){
			return $this->CommonClass1->NormalQuery("UPDATE `jobs` SET `job_position` ='".$data['job_position']."',`academic_session`='".$data['academic_session']."',`job_position_number`='".$data['job_position_number']."',`job_experience`='".$data['job_experience']."',`job_function`='".$data['job_function']."',`job_description`='".$data['job_description']."',`job_responsibilities`='".$data['job_responsibilities']."',`job_skills`='".$data['job_skills']."',`job_qualification`= '".$data['job_qualification']."' WHERE id=".$data['hiddenval']." ");
			
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `jobs`(`job_position`, `academic_session`, `job_position_number`, `job_experience`, `job_function`, `job_description`, `job_responsibilities`, `job_skills`, `job_qualification`)VALUES ('".$data['job_position']."','".$data['academic_session']."','".$data['job_position_number']."','".$data['job_experience']."','".$data['job_function']."','".$data['job_description']."','".$data['job_responsibilities']."','".$data['job_skills']."','".$data['job_qualification']."')");
	}
	function GetJobsList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM jobs  WHERE status != 2 ORDER BY id DESC ";
						
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['job_position']."</td><td>".$category['academic_session']."</td><td>".$category['job_position_number']."</td><td>".$category['job_experience']."</td><td>".$category['job_function']."</td><td>".$category['job_description']."</td><td>".$category['job_responsibilities']."</td><td>".$category['job_skills']."</td><td>".$category['job_qualification']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-jobs.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'jobs',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'jobs',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
	function addNoticeData($data){
		if($data['hiddenval']!=''){
			return $this->CommonClass1->NormalQuery("UPDATE `notice` SET `heading` ='".$data['heading']."',`description`='".$data['description']."' WHERE id=".$data['hiddenval']." ");
			
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `notice`(`heading`, `description`)VALUES ('".$data['heading']."','".$data['description']."')");
	}
	function GetNoticeList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM notice  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['heading']."</td><td>".$category['description']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-notice.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'notice',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'notice',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}
	
	function addFacultyData($data){
		$result='';
		if(!empty($data['hiddenval'])){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			$result= $this->CommonClass1->NormalQuery("UPDATE `faculty` SET `name`='".$data['name']."',`designation`='".$data['designation']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else{
			$result= $this->CommonClass1->NormalQuery("INSERT INTO `faculty`(`name`,`designation`,`image`) VALUES ('".$data['name']."','".$data['designation']."','".$data['image']."') ");
		}
		return ($result)?true:false;
	}
	function GetFacultyList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM faculty  WHERE status != 2 ORDER BY id DESC ";
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
			foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td><img src='uploads/faculty/".$category['image']."'></td><td>".$category['name']."</td><td>".$category['designation']."</td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-faculty.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'faculty',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'faculty',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

	function addDirectorData($data){
		if($data['hiddenval']!=''){
			$image=!empty($data['image'])?",image='".$data['image']."'":'';
			return $this->CommonClass1->NormalQuery("UPDATE `director` SET `heading`='".$data['heading']."',`short_message`='".$data['short_message']."',`long_message`='".$data['long_message']."' ".$image." WHERE id=".$data['hiddenval']." ");
		}
		else
				return $this->CommonClass1->NormalQuery("INSERT INTO `director`(`heading`,`short_message`,`long_message`,`image`) VALUES ('".$data['heading']."','".$data['short_message']."','".$data['long_message']."','".$data['image']."') ");
	}
	function GetDirectorList(){
		$categoryData='';
						$query	="SELECT *, case when status='1' then 'Active' else 'DeActive' END AS status,case when status='1' then 'act fa fa-circle' else 'dct fa fa-circle' end as class  FROM director  WHERE status != 2 ORDER BY id DESC ";;
		
		$categorys = $this->CommonClass1->ResultWithArrayData($query);
			$count=1;
			if(!empty($categorys)){
				foreach ($categorys as $key => $category) {
					$categoryData.="<tr><td>".$count."</td><td>".$category['heading']."</td><td>".$this->CommonClass1->CountWord(7,$category['short_message'])."</td><td><img src='uploads/director/".$category['image']."'></td><td>".$category['status']."</td><td class='action'><a class='showact' href='javascript:' onclick='showHideAct(this);'></a><div class=''><a title='Edit/Update'href='add-director.php?id=".$category['id']."' class=\"btn btn-warning btn-sm\"><i class=\"fa fa-pencil\"></i></a><button onclick=\"ActivateDeActivateThisRecord(this,'director',".$category['id'].");\" class='btn btn-sm active' title='Active/DeActive'><span class='".$category['class']."'></span></button><button onclick=\"delete_row(this,'director',".$category['id'].");\" class='btn btn-danger btn-sm del' title='Delete'><span class='fa fa-trash-o'></span></button><div></td></tr>";
				$count++;
			}
		}else
			$categoryData.='<tr style="text-align:center;font-size:18px;" ><td colspan="7" class="alert-danger">No Record Found</td></tr>';
		return $categoryData;
	}

}
?>