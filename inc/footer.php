<section class="footer">
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="footer_wrap">
              <div class="footer_wrap_text">
                <h6> About Road Lines</h6>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
              </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="footer_wrap">
            <div class="footer_link">
              <h6>Our Services</h6>
            </div>
            <ul>
              <li><a href="#">Land Transport</a></li>
              <li><a href="#">Ware House</a></li>
              <li><a href="#">Cargo Transport</a></li>
              <li><a href="#">Packaging & Storage</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-3">
            <div class="footer_wrap">
              <div class="footer_link">
                <h6>Ouick Link</h6>
              </div>
              <ul>
                <li><a href="#">About US</a></li>
                <li><a href="#">Services</a></li>
                <li><a href="#">Contact Us</a></li>
                <li><a href="#">Privacy</a></li>
              </ul>
            </div>
        </div>
        <div class="col-md-3">
         <div class="foot_right_link">
           <div class="foot_right_text">
             <h6>Get In Touch</h6>   
           </div>
           <div class="text_link_wrap">
              <i class="fas fa-phone fa-flip-horizontal"></i>
            <a href="#">+91 8758962354</a>
           </div>
           <div class="text_link_wrap">
              <i class="far fa-envelope"></i>
              <a href="mailto:roadlines@gmail.com">roadlines@gmail.com</a>
             </div>
           
         </div>
        </div>
      </div>
      <div class="footer_bootom">
        <div class="row">
          <div class="col-md-6">
            <div class="foot_bootom_left_text">
            <p>© 2019 Transportation Logistics All rights reserved</p>
            </div>
          </div>
           <div class="col-md-6">
              <div class="foot_bottom_social">
                  <ul>
                      <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                      <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                      <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                      <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                  </ul>
                </div>
           </div>
        </div>
      </div>
    </div>
  </section>


  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init({
      duration: 1200,
    })
  </script>
  <script src="assets/js/main.js"></script>
</body>

</html>