<?php include('inc/header.php')?>
<?php include('inc/config.php')?>

<section class="hero_slider">
    <div class="owl-carousel owl-theme slider_carousel">
      <div class="item">
        <div class="slider_img" style="background-image:url(assets/img/slider_1.jpg)">
          <div class="slider_content">
            <h3 data-aos="fade-right" data-aos-duration="1500">We are TATA MOTORS authorised service centre. </h3>
            <!-- <p data-aos="fade-right" data-aos-duration="1500">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.
            </p> -->
            <a href="about.php" data-aos="fade-right" data-aos-duration="1500">MORE ABOUT</a>
          </div>
          <div class="social_icon_link">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <!-- <li><a href="#"><i class="fab fa-instagram"></i></a></li> -->
              <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
              <!-- <li><a href="#"><i class="fab fa-youtube"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="slider_img" style="background-image:url(assets/img/slider_2.jpg)">
          <div class="slider_content">
            <h3> We are the biggest authorised service centre of TATA MOTORS in north region. </h3>
            <!-- <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
              dolore magna aliqua.
            </p> -->
            <a href="about.php">MORE ABOUT</a>
          </div>
          <div class="social_icon_link">
            <ul>
              <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
              <!-- <li><a href="#"><i class="fab fa-instagram"></i></a></li> -->
              <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
              <!-- <li><a href="#"><i class="fab fa-youtube"></i></a></li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="about_company">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="about_company_bg aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">
            <img src="assets/img/pic4.jpg" alt="">
          </div>
        </div>
        <div class="col-md-7">
          <div class="about_content_wrap">
            <div class="about_yers_sec aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">
              <span class="years_number">25</span>
              <span class="years_content_large">Years of Experience</span>
            </div>
            <div class="about_bottom_content">
              <h4 class="aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">Improving quality of service with an integrated unified approach.</h4>
              <p class="aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">  </p>
              <p class="aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">  </p>
            </div>
            <a href="about.php" class="more__about_btn hover_btn_effect aos-init aos-animate"  data-aos="fade-up" data-aos-duration="1500">More About</a>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="service_sec">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4 class="text_overlay aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">
            Our Services
          </h4>
          <h2 class="servicee_title aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">Our Services</h2>
          <p class="service_desc aos-init aos-animate" data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
            incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
        </div>
        <div class="col-md-3">
          <div class="service_box_wrap">
            <div class="service_icon" data-aos="fade-up" data-aos-duration="1500">
              <i class="flaticon-truck"></i>
            </div>
            <div class="service_box_content">
              <h4 data-aos="fade-up" data-aos-duration="1500"> Land Transport </h4>
              <p data-aos="fade-up" data-aos-duration="1500"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur labore
                dolore</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="service_box_wrap">
            <div class="service_icon" data-aos="fade-up" data-aos-duration="1500">
              <i class="flaticon-data-warehouse"></i>
            </div>
            <div class="service_box_content">
              <h4 data-aos="fade-up" data-aos-duration="1500"> Warehousing </h4>
              <p data-aos="fade-up" data-aos-duration="1500"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur labore
                dolore</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="service_box_wrap">
            <div class="service_icon" data-aos="fade-up" data-aos-duration="1500">
              <i class="flaticon-box-truck"></i>
            </div>
            <div class="service_box_content">
              <h4 data-aos="fade-up" data-aos-duration="1500"> Door to Door Service</h4>
              <p data-aos="fade-up" data-aos-duration="1500"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur labore
                dolore</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="service_box_wrap">
            <div class="service_icon" data-aos="fade-up" data-aos-duration="1500">
              <i class="flaticon-crane"></i>
            </div>
            <div class="service_box_content">
              <h4 data-aos="fade-up" data-aos-duration="1500">  Logistic Solutions </h4>
              <p data-aos="fade-up" data-aos-duration="1500"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed dotempor incididunt consectetur labore
                dolore</p>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="show_btn" data-aos="fade-up" data-aos-duration="1500">
            <a href="service.php"class="hover_btn_effect" >Show all <span class="ti-arrow-right"></span></a>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="extra_sec">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="extra_sec_title">
            <h5 data-aos="fade-up" data-aos-duration="1500">ARE YOU READY</h5>
            <h3 data-aos="fade-up" data-aos-duration="1500">TO MOVE?</h3>
            <p data-aos="fade-up" data-aos-duration="1500">We deliver the goods to the most complicated places on earth</p>
          </div>
          <div class="offer_sec" data-aos="fade-right " data-aos-duration="1500">
            <img src="assets/img/offer.jpg" alt="">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="how_work">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500">
            How we Work
          </h4>
          <h2 class="servicee_title" data-aos="fade-up" data-aos-duration="1500">How we Work</h2>
          <p class="how_desc" data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
            ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
        </div>
        <div class="col-md-3">
          <div class="how_work_box">
            <div class="how_work_bg" data-aos="fade-up" data-aos-duration="1500">
              <img src="assets/img/booking-1.png" alt="">
            </div>
            <div class="how_work_box_titlte">
              <h5 data-aos="fade-up" data-aos-duration="1500">01. Book Us</h5>
              <p data-aos="fade-up" data-aos-duration="1500">Randomised words which don’t look even slightly believable.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="how_work_box">
            <div class="how_work_bg" data-aos="fade-up" data-aos-duration="1500">
              <img src="assets/img/packing-1.png" alt="">
            </div>
            <div class="how_work_box_titlte">
              <h5 data-aos="fade-up" data-aos-duration="1500">02. We Pack your Goods</h5>
              <p data-aos="fade-up" data-aos-duration="1500">Randomised words which don’t look even slightly believable.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="how_work_box">
            <div class="how_work_bg" data-aos="fade-up" data-aos-duration="1500">
              <img src="assets/img/shiping-1.png" alt="">
            </div>
            <div class="how_work_box_titlte">
              <h5 data-aos="fade-up" data-aos-duration="1500">03. We Move your Goods</h5>
              <p data-aos="fade-up" data-aos-duration="1500">Randomised words which don’t look even slightly believable.</p>
            </div>
          </div>
        </div>
        <div class="col-md-3">
          <div class="how_work_box">
            <div class="how_work_bg" data-aos="fade-up" data-aos-duration="1500">
              <img src="assets/img/delivery-1.png" alt="">
            </div>
            <div class="how_work_box_titlte">
              <h5 data-aos="fade-up" data-aos-duration="1500">01. Book Us</h5>
              <p data-aos="fade-up" data-aos-duration="1500">Randomised words which don’t look even slightly believable.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="goodness">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="goodness_title">
            <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500">
              Why Choose Us
            </h4>
            <h2 class="servicee_title" data-aos="fade-up" data-aos-duration="1500"> Why Choose Us</h2>
            <p class="how_desc" data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
              incididunt ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
          </div>
        </div>
        <div class="col-md-6">
          <div class="goodness_bg" data-aos="fade-right" data-aos-duration="1500">
            <img src="assets/img/courier-man.png" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <div class="goodness_box" data-aos="fade-up" data-aos-duration="1500">
                <div class="media">
                  <div class="goodness_icon">
                    <i class="flaticon-package-for-delivery"></i>
                  </div>
                  <div class="goodness_content media-body">
                    <span>Packaging and Storage</span>
                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Praesent
                      pellentesque diam vitae nibh aliquam faucibus.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="goodness_box" data-aos="fade-up" data-aos-duration="1500">
                <div class="media">
                  <div class="goodness_icon">
                    <i class="flaticon-shield-1"></i>
                  </div>
                  <div class="goodness_content media-body">
                    <span>Safety & Quality</span>
                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Praesent
                      pellentesque diam vitae nibh aliquam faucibus.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="goodness_box" data-aos="fade-up" data-aos-duration="1500">
                <div class="media">
                  <div class="goodness_icon">
                    <i class="flaticon-tree-with-many-leaves"></i>
                  </div>
                  <div class="goodness_content media-body">
                    <span>Care for Environment</span>
                    <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae. Praesent
                      pellentesque diam vitae nibh aliquam faucibus.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="testimonial">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h4 class="text_overlay" data-aos="fade-up" data-aos-duration="1500">
            Testimonials
          </h4>
          <h2 class="servicee_title" data-aos="fade-up" data-aos-duration="1500">Testimonials</h2>
          <p class="how_desc" data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
            ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
        </div>
      </div>
      <div class="owl-carousel owl-theme testimonial_home">
        <div class="item">
          <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
            <div class="media">
              <div class="client_pic">
                <img src="assets/img/team_1.jpg" alt="">
              </div>
              <div class="client_inner_content media-body">
                <p>
                  Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                  the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                </p>
                <div class="client_footer">
                  <span>John Gerry Design Hunt</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
            <div class="media">
              <div class="client_pic">
                <img src="assets/img/team_3.jpg" alt="">
              </div>
              <div class="client_inner_content media-body">
                <p>
                  Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                  the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                </p>
                <div class="client_footer">
                  <span>John Gerry Design Hunt</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
            <div class="media">
              <div class="client_pic">
                <img src="assets/img/team_1.jpg" alt="">
              </div>
              <div class="client_inner_content media-body">
                <p>
                  Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                  the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                </p>
                <div class="client_footer">
                  <span>John Gerry Design Hunt</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="testimonial_box bg_wave" data-aos="fade-up" data-aos-duration="1500">
            <div class="media">
              <div class="client_pic">
                <img src="assets/img/team_3.jpg" alt="">
              </div>
              <div class="client_inner_content media-body">
                <p>
                  Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live
                  the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                </p>
                <div class="client_footer">
                  <span>John Gerry Design Hunt</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="top__foot">
    <div class="container custom__container">
      <div class="row align-items-center">
        <div class="col-md-5">
          <div class="news__letter_content">
            <h6  data-aos="fade-up" data-aos-duration="1500" >Subscribe Our Newsletter</h6>
            <p  data-aos="fade-up" data-aos-duration="1500">Luisque dapibus lacus non pulvinar lobo.</p>
          </div>
        </div>
        <div class="col-md-7">
          <form action="#">
            <div class="news_for">
              <input class="form__control_news" placeholder="Enter your email" type="email">
              <button type="submit" class="btn newsletter__btn_sumbit blob-small">Subscribe</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  <?php include('inc/footer.php')?>