<?php include('inc/header.php')?>
<?php include('inc/config.php')?>

<section class="breadcumb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcumb_title">
                    <h1>Testimonial</h1>
                    <ul class="breadcrumb_style_2">
                        <li><a href="index.php">Home</a><span class="ti-angle-double-right"
                                id="breadcumb_sperator_icon"></span></li>
                        <li>Testimonial</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial_more_sec">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text_overlay"  data-aos="fade-up" data-aos-duration="1500">
                    Testimonials
                </h4>
                <h2 class="servicee_title"  data-aos="fade-up" data-aos-duration="1500">Testimonials</h2>
                <p class="how_desc"  data-aos="fade-up" data-aos-duration="1500">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt
                    ut labore et dolore magna aliqua. Egestas purus viverra accumsan in nisl nisi.</p>
            </div>
            <div class="col-md-6">
                <div class="testimonial_box bg_wave inner_testimonial_adjust"  data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_1.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="testimonial_box bg_wave inner_testimonial_adjust"  data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_3.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="testimonial_box bg_wave inner_testimonial_adjust"  data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_1.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="testimonial_box bg_wave inner_testimonial_adjust"  data-aos="fade-up" data-aos-duration="1500">
                    <div class="media">
                        <div class="client_pic">
                            <img src="assets/img/team_3.jpg" alt="">
                        </div>
                        <div class="client_inner_content media-body">
                            <p>
                                Far far away, behind the word mountains, far from the countries Vokalia and Consonantia,
                                there live
                                the blind texts. Aliquam gravida, urna quis ornare imperdiet,
                            </p>
                            <div class="client_footer">
                                <span>John Gerry Design Hunt</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php')?>