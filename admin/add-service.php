<?php
session_start();
if($_SESSION["adminId"]!=1)
header('Location: index.php');
?>
<?php include('include/navbar.php'); ?>
<!-- partial -->
<?php include('include/sidebar.php'); ?>
<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])){
$id=$_REQUEST['id'];
$editData=$CommonClass->ResultWithSingleRow("SELECT * FROM about WHERE id=".$id."");
}
?>
<!-- partial -->
<div class="content-wrapper">
  <div class="row">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Add About Details</h4>
            <form class="forms-sample formcontrol-area" role="form" enctype="multipart/form-data"  onsubmit="AddAboutData(this,event,'.form_validation');">
           
            <div>
              <label for="description">Description</label>
              <textarea class="form-control summernote" name="description" id="description" placeholder="Description"  required tabindex="3" rows="10"><?=(!empty($editData['description']))?$editData['description']:''; ?> </textarea>
              </div>
              
              
              <div class="form-group" title="Upload facility icon image">
                <input type="file" name="image1" id="image1" tabindex="4" onchange="fileuploadpreview(this);" class="file-upload-browse btn btn-info" <?php echo !empty($editData['image1'])?'':'required' ?>>
                <div class="img-responsive previewimg">
                  <?=( !empty($editData['image1']) )? '<img width="100px" src="'.SITE_URL.'admin/uploads/about/'.$editData['image1'].'">':'' ;?>
                </div>
              </div>

              <div class="form-group" title="Upload facility icon image">
                <input type="file" name="image2" id="image2" tabindex="4" onchange="fileuploadpreview(this);" class="file-upload-browse btn btn-info" <?php echo !empty($editData['image2'])?'':'required' ?>>
                <div class="img-responsive previewimg">
                  <?=( !empty($editData['image2']) )? '<img width="100px" src="'.SITE_URL.'admin/uploads/about/'.$editData['image2'].'">':'' ;?>
                </div>
              </div>
             
              <div style="clear: both;"></div>
              <input type="hidden" name="action" value="add_about_data">
              <input type="hidden" name="hiddenval" id="hiddenval" value="<?=!empty($editData['id'])?$editData['id']:"";?>" >
              <div style="clear:both;"></div>
              <div class="col-12 form-group">
                <button type="button" class="btn btn-success mr-2 form_validation" tabindex="2">Submit</button>
                <a href="<?php echo SITE_URL ?>admin/about-list.php" tabindex="3"><button type="button" class="btn btn-light">Cancel</button></a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php include('include/footer.php'); ?>
  <?php include('include/script.php'); ?>
 <script type="text/javascript">

        $('.summernote').summernote({

          height: 120,   //set editable area's height

        });

      </script>

  <style type="text/css">

    .form-group{

   width: 462px;

    float: left;

    }

    .popover-content.note-children-container{

      display: none;

    }

  </style>