/*Show Path Url*/
var DASHURL =  SITE_URL+'admin/';
//Function Call On Load*/
$(document).ready(function(){
$('#media_type').trigger('onchange');
GetDataOfThisPage();
setActiveClass();
/*-------------- Form Validation -----------*/
$('.form_validation').on('click', function(e){
e.preventDefault();
var btn = $(this).text();
if($(this).text()=='...Processing')
return false;
$(this).text('...Processing');
var chk=0;
var obj=$(this).closest('form');
if( TextBoxValidation(obj) === false )
chk=1;
if(chk===1){
$(this).text(btn);
return false;
}
obj.submit();
});
$('.loginValidation').on('click', function(e){
e.preventDefault();
var btn = $(this).text();
if($(this).text()=='...Processing')
return false;
$(this).text('...Processing');
var chk=0;
var obj=$(this).closest('form');
if( TextBoxValidation(obj) === false ){
chk=1;
}
if(chk===1){
$(this).text(btn);
return false;
}else{
obj.submit();
}
});
});
/************Calling function according to current page**********/
function GetDataOfThisPage() {
var uriArray=returnUriArray();
var index=uriArray.length-1;
var lastVal=uriArray[index];
if($.inArray("slider-list.php", uriArray)>-1){
GetSliderList();
}
else if($.inArray("refrigerator-list.php", uriArray)>-1){
GetRefrigeratorList();
}
else if($.inArray("image-list.php", uriArray)>-1){
GetGalleryList();
}
else if($.inArray("testimonial-list.php", uriArray)>-1){
GetTestimoniaList();
}
else if($.inArray("about-list.php", uriArray)>-1){
GetAboutList();
}
else if($.inArray("enquiry-list.php", uriArray)>-1){
GetEnquiryList();
}
else if($.inArray("downloadpros-list.php", uriArray)>-1){
GetDownloadprosList();
}
else if($.inArray("achievers-list.php", uriArray)>-1){
GetAchieversList();
}
else if($.inArray("mainnews-list.php", uriArray)>-1){
GetHotnewsList();
}
else if($.inArray("services-list.php", uriArray)>-1){
GetServicesList();
}
else if($.inArray("chairman-list.php", uriArray)>-1){
GetChairmanList();
}
else if($.inArray("notice-list.php", uriArray)>-1){
GetNoticeList();
}
else if($.inArray("image-list.php", uriArray)>-1){
GetGalleryList();
}
else if($.inArray("calender-list.php", uriArray)>-1){
GetCalenderList();
}
else if($.inArray("download-list.php", uriArray)>-1){
GetDownloadList();
}
else if($.inArray("tc-list.php", uriArray)>-1){
GetTcList();
}
else if($.inArray("reg-list.php", uriArray)>-1){
GetRegList();
}
else if($.inArray("popup-list.php", uriArray)>-1){
GetPopupList();
}
else if($.inArray("contact-list.php", uriArray)>-1){
GetContactList();
}
else if($.inArray("addmission-list.php", uriArray)>-1){
GetAddmissionList();
}
else if($.inArray("carrer-list.php", uriArray)>-1){
GetCarrerList();
}
else if($.inArray("gallery-detail-list.php", uriArray)>-1){
GetGalleryDetailList();
}
else if($.inArray("video-detail-list.php", uriArray)>-1){
GetVideoDetailList();
}
else if($.inArray("video-list.php", uriArray)>-1){
GetVideoList();
}
else if($.inArray("payment-list.php", uriArray)>-1){
GetPaymentList();
}
else if($.inArray("principal-list.php", uriArray)>-1){
GetPrincipalList();
}
else if($.inArray("director-list.php", uriArray)>-1){
GetDirectorList();
}
else if($.inArray("newss-list.php", uriArray)>-1){
GetNewssList();
}
else if($.inArray("news-list.php", uriArray)>-1){
GetNewsList();
}
else if($.inArray("serviceform-list.php", uriArray)>-1){
GetServiceformList();
}
else if($.inArray("mediacoverage-list.php", uriArray)>-1){
GetMediaList();
}
else if($.inArray("jobs-list.php", uriArray)>-1){
GetJobsList();
}
else if($.inArray("faculty-list.php", uriArray)>-1){
GetFacultyList();
}
}
/************active deactive records**********/
function ActivateDeActivateThisRecord(obj, tableName,id) {
var $tr = $(obj).closest('tr');
var index = $tr.index();
$newindex=$('.table').find('tbody tr:eq('+parseInt(index)+')').find('td:last').index();
$status=$('.table').find('tbody tr:eq('+parseInt(index)+')').find('td:eq('+parseInt($newindex-1)+')').html();
$status=($status.trim()=='Active')?0:1;
var action = "CallHandlerForActivatedRecord(" + id + "," + index + ",'" + tableName + "',"+$status+");";
action = '"' + action + '"';
var $div = "<div id='sb-containerDel'><div id='sb-wrapper' style='width:425px'><h1 style='font-size: 20.5px;'>Are you sure want to active/deactive this record ?</h1><hr/><table><tr><td><a href='javascript:void(0)' id='deleteyes' onclick=" + action + " class='button'>"
    + "Yes</a></td><td><a href='javascript:void(0)' onclick='RemoveDelConfirmDiv();' class='button_deact fr'>No</a></td></tr></table></div></div>";
    $('body').append($div);
    $('#sb-containerDel').show('slow');
    }
    function CallHandlerForActivatedRecord(id,index, tab,status) {
    $('#deleteyes').html('..Processing');
    $.ajax({
    url: DASHURL+'adminAjax.php',
    type: "POST",
    data: {action:'Change_Status',id:id,tab:tab,status:status},
    dataType: "text",
    success: function (d) {
    $newindex=$('.table').find('tbody tr:eq('+parseInt(index)+')').find('td:last').index();
    /*Change Status Text*/
    $objstatustxt=$('.table').find('tbody tr:eq('+parseInt(index)+')').find('td:eq('+parseInt($newindex-1)+')');
    var statustxt=(status==1)?'Active':'DeActive';
    $objstatustxt.html(statustxt);
    /*Change Class*/
    $objlast=$('.table').find('tbody tr:eq('+parseInt(index)+')').find('td:last').find('span:eq(0)');
    var statuscls=(status==1)?'act fa fa-circle':'dct fa fa-circle';
    $objlast.removeClass('class');
    $objlast.attr('class',statuscls);
    RemoveDelConfirmDiv();
    }
    });
    }
    /*-------------- Delete Records -------------------*/
    function delete_row(obj,tab,id){
    var  $tr=$(obj).closest('tr');
    var index=$tr.index();
    var action = "CallHandlerForDeleteRecord(" + id + "," + index + ",'" + tab + "');";
    action = '"' + action + '"';
    var $div = "<div id='sb-containerDel'><div id='sb-wrapper' style='width:428px'><h1 style='font-size: 20.5px;'>Are you sure want to delete this record ?</h1><hr/><table><tr><td><a href='javascript:void(0)' id='deleteyes' onclick=" + action + " class='button'>"
        + "Yes</a></td><td><a href='javascript:void(0)' onclick='RemoveDelConfirmDiv();' class='button_deact fr'>No</a></td></tr></table></div></div>";
        $('body').append($div);
        $('#sb-containerDel').show('slow');
        }
        function CallHandlerForDeleteRecord(id,index, tab) {
        $('#deleteyes').html('..Processing');
        var status=2;
        var formData={action:"Delete_Record",tab:tab,id:id,status:status};
        $.ajax({
        url: DASHURL+'adminAjax.php',
        type: "POST",
        data: formData,
        success: function (d) {
         var d = JSON.parse(d);
         if(d.valid){         
        var $ntr = $('.table').find('tbody').find('tr:eq(' + index + ')');
        $ntr.remove();
        RemoveDelConfirmDiv();
        }
        },error(d){}
        });
        }
        function travellerAdminLogin(obj,e,btn){
        e.preventDefault();
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if( response.valid){
        $ .alert("Auto Redirecting after: ", {withTime: true,type: 'success',title:'Successfully Login',icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        setTimeout(function(){
        window.location.href=DASHURL+"dashboard.php";
        },3000);
        }else{
        $ .alert("Please check your login details.", {title:'Invalid login credential!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }

        function AddSliderData(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"slider-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }


        function updateCategory(obj,id){
        $tr=$(obj).closest('tr');
        $('#trIndex').val($tr.index());
        $('#hiddenval').val(id);
        $('#name').val($tr.find('td:eq(0)').text().replace(/'/g,""));
        $('.cat_validation').text('Update');
        $('h3.m-b-none').text('Update Teacher Category');
        }
        function AddAboutData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"about-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddPrincipalData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"principal-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }

        function AddDirectorData(obj,e,btn) {
        
            e.preventDefault();
            var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
            $.ajax({
            url         : DASHURL+'adminAjax.php',
            type        : "POST",
            data        : new FormData(obj),
            dataType: "json",
            contentType : false,
            cache       : false,
            processData : false,
            success:function(response){ 
            if(response==1){
            $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
            ResetTextBox(obj);
            $('#hiddenval').val('');
            $("#image").val('');
            $('.previewimg').html('');
            setTimeout(function(){
            window.location.href=DASHURL+"director-list.php";
            },2000);
            }
            $(btn).text('Submit');
            
            },error:function(response){}
            });
            }

        function AddFacultyData(obj,e,btn) {
        
            e.preventDefault();
            var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
            $.ajax({
            url         : DASHURL+'adminAjax.php',
            type        : "POST",
            data        : new FormData(obj),
            dataType: "json",
            contentType : false,
            cache       : false,
            processData : false,
            success:function(response){ 
            if(response==1){
            $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
            ResetTextBox(obj);
            $('#hiddenval').val('');
            $("#image").val('');
            $('.previewimg').html('');
            setTimeout(function(){
            window.location.href=DASHURL+"faculty-list.php";
            },2000);
            }
            $(btn).text('Submit');
            
            },error:function(response){}
            });
            }

        function AddHotnewsData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"mainnews-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddChairmanData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"chairman-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddCalenderData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"calender-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddDownloadData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"download-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
           function AddDownloadprosData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"downloadpros-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddPopupData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"popup-list.php";
        },2000);
        }
        $(btn).text('Submit');
        },error:function(response){}
        });
        }
        function AddNoticeDetails(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"notice-list.php";
        },2000);
        }
        $(btn).text('Submit');
        },error:function(response){}
        });
        }
        function AddNewssData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"newss-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddFeaturedNews(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"news-list.php";
        },2000);
        }
        $(btn).text('Submit');
        },error:function(response){}
        });
        }

        function AddGalleryData(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"image-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }

        function AddGalleryDetails(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"gallery-detail-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddVideoDetails(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"video-detail-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddVideo(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"video-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }

        function AddJobData(obj,e,btn) {
            e.preventDefault();
            var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
            $.ajax({
            url         : DASHURL+'adminAjax.php',
            type        : "POST",
            data        : new FormData(obj),
            dataType: "json",
            contentType : false,
            cache       : false,
            processData : false,
            success:function(response){
            if(response==1){
            $ .alert("Auto Redirecting jobs list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
            ResetTextBox(obj);
            $('#hiddenval').val('');
            $("#sliderImg").val('');
            $('.previewimg').html('');
            setTimeout(function(){
            window.location.href=DASHURL+"jobs-list.php";
            },2000);
            }
            else if(response==2){
            $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
            }
            $(btn).text('Submit');
            
            },error:function(response){}
            });
            }
            function AddVideo(obj,e,btn) {
            e.preventDefault();
            var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
            $.ajax({
            url         : DASHURL+'adminAjax.php',
            type        : "POST",
            data        : new FormData(obj),
            dataType: "json",
            contentType : false,
            cache       : false,
            processData : false,
            success:function(response){
            if(response==1){
            $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
            ResetTextBox(obj);
            $('#hiddenval').val('');
            $("#sliderImg").val('');
            $('.previewimg').html('');
            setTimeout(function(){
            window.location.href=DASHURL+"video-list.php";
            },2000);
            }
            else if(response==2){
            $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
            }
            $(btn).text('Submit');
            
            },error:function(response){}
            });
            }


        function AddTcDetails(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"tc-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        function AddRegistrationForm(obj,e,btn) {
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){
        if(response==1){
        $ .alert("Auto Redirecting slider list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#sliderImg").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"reg-list.php";
        },2000);
        }
        else if(response==2){
        $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
          function AddAchieversData(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
            url         : DASHURL+'adminAjax.php',
            type        : "POST",
            data        : new FormData(obj),
            dataType: "json",
            contentType : false,
            cache       : false,
            processData : false,
            success:function(response){
            if(response==1){
            $ .alert("Auto Redirecting jobs list: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
            ResetTextBox(obj);
            $('#hiddenval').val('');
            $("#sliderImg").val('');
            $('.previewimg').html('');
            setTimeout(function(){
            window.location.href=DASHURL+"achievers-list.php";
            },2000);
            }
            else if(response==2){
            $ .alert("Please check your Order details.", {title:'Order No Already exists!',type: 'warning',icon:'glyphicon glyphicon-heart',position: ['top-left', [-0.42, 0]]});
            }
            $(btn).text('Submit');
            
            },error:function(response){}
            });
            }
        function AddTestimonialDetails(obj,e,btn) {
        
        e.preventDefault();
        var id = ($('#hiddenval').val().length >0)?$('#hiddenval').val():"";
        $.ajax({
        url         : DASHURL+'adminAjax.php',
        type        : "POST",
        data        : new FormData(obj),
        dataType: "json",
        contentType : false,
        cache       : false,
        processData : false,
        success:function(response){ 
        if(response==1){
        $ .alert("Auto Redirecting On List: ", {withTime: true,type: 'success',title:id!==''?"Update Successfully":"Successfully Added",icon:'glyphicon glyphicon-heart',minTop: 0});
        ResetTextBox(obj);
        $('#hiddenval').val('');
        $("#image").val('');
        $('.previewimg').html('');
        setTimeout(function(){
        window.location.href=DASHURL+"testimonial-list.php";
        },2000);
        }
        $(btn).text('Submit');
        
        },error:function(response){}
        });
        }
        
        function addDetailBox(obj){
        var html=$(obj).closest('.main_detail_box').find('.col-12').html();
        $('.main_detail_box').append('<div class="col-12 form-group">'+html+'</di>');
        $(obj).closest('.main_detail_box').find('.summernote').text('');
        }

            function removeDetailBox(obj){
            
            $(obj).closest('.col-12').remove();
            }
            /*-------- get Category list -----------*/
            function GetSliderList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetSliderList');
            sendAjaxRequest(formData);
            }
            function GetAboutList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetAboutList');
            sendAjaxRequest(formData);
            }
            function GetRefrigeratorList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetRefrigeratorList');
            sendAjaxRequest(formData);
            }
            function GetHotnewsList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetHotnewsList');
            sendAjaxRequest(formData);
            }
            function GetTestimoniaList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="9"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetTestimoniaList');
            sendAjaxRequest(formData);
            }
            function GetPrincipalList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetPrincipalList');
            sendAjaxRequest(formData);
            }
            function GetDirectorList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetDirectorList');
            sendAjaxRequest(formData);
            }
            function GetDownloadprosList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetDownloadprosList');
            sendAjaxRequest(formData);
            }
            function GetCalenderList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetCalenderList');
            sendAjaxRequest(formData);
            }
            function GetPopupList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetPopupList');
            sendAjaxRequest(formData);
            }
            function GetDownloadList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetDownloadList');
            sendAjaxRequest(formData);
            }
            function GetChairmanList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetChairmanList');
            sendAjaxRequest(formData);
            }
            function GetTcList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetTcList');
            sendAjaxRequest(formData);
            }
            function GetRegList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetRegList');
            sendAjaxRequest(formData);
            }
            function GetNewsList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetNewsList');
            sendAjaxRequest(formData);
            }
            function GetNoticeList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetNoticeList');
            sendAjaxRequest(formData);
            }
            function GetNewssList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetNewssList');
            sendAjaxRequest(formData);
            }

            function GetGalleryList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetGalleryList');
            sendAjaxRequest(formData);
            }
            function GetGalleryDetailList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetGalleryDetailList');
            sendAjaxRequest(formData);
            }
            function GetVideoDetailList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetVideoDetailList');
            sendAjaxRequest(formData);
            }
            function GetVideoList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetVideoList');
            sendAjaxRequest(formData);
            }
            function GetJobsList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetJobsList');
            sendAjaxRequest(formData);
            }
            function GetFacultyList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetFacultyList');
            sendAjaxRequest(formData);
            }
            function GetPaymentList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetPaymentList');
            sendAjaxRequest(formData);
            }
            function GetFeedbackList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetFeedbackList');
            sendAjaxRequest(formData);
            }
            function GetAchieversList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetAchieversList');
            sendAjaxRequest(formData);
            }
            function GetServicesFormList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetServicesFormList');
            sendAjaxRequest(formData);
            }
            function GetAddmissionList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetAddmissionList');
            sendAjaxRequest(formData);
            }
            function GetCarrerList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetCarrerList');
            sendAjaxRequest(formData);
            }
            function GetContactList(){
            $('tbody').html('<tr style="text-align:center"><td colspan="5"><img width=100px; src="'+ DASHURL+'images/loading.gif" alt="loading" class="loader"></td></tr>');
            var formData = createSearchCondition('GetContactList');
            sendAjaxRequest(formData);
            }
            /*-------- ajax code for common use -----------*/
            function sendAjaxRequest(formData){
            var baseurl=DASHURL+'adminAjax.php';
            $.ajax({
            type: "POST",
            url: baseurl,
            data: formData,
            success: function (data) {
            // console.log(data);
            var data = JSON.parse(data);
           
            if(data.valid){
            $('.table').find('.loader').css('display','none');
            $('.table').find('tbody').html(data.id);
            if($("#tableDataTable").length>0 && $('#tableDataTable').find('tbody tr:eq(0)').find('td.alert-danger').length==0)
            $("#tableDataTable").dataTable();
            }
            },error: function(data){}
            });
            }
            /*-------- Create Search Condition list -----------*/
            function createSearchCondition(cat){
            return (((cat=='GetSliderList')?{action: "Get_Slider_List"}:((cat=='GetTestimoniaList')?{action: "Get_Testimonial_List"}:((cat=='GetAboutList')?{action: "Get_About_List"}:((cat=='GetGalleryList')?{action: "Get_Gallery_List"}:((cat=='GetServicesFormList')?{action: "Get_Serviceform_List"}:((cat=='GetContactList')?{action: "Get_Contact_List"}:((cat=='GetTcList')?{action: "Get_Tc_List"}:((cat=='GetPrincipalList')?{action: "Get_Principal_List"}:((cat=='GetDirectorList')?{action: "Get_Director_List"}:((cat=='GetAchieversList')?{action: "Get_Achievers_List"}:((cat=='GetNewsList')?{action: "Get_News_List"}:((cat=='GetPaymentList')?{action: "Get_Payment_List"}:((cat=='GetGalleryList')?{action: "Get_Gallery_List"}:((cat=='GetGalleryDetailList')?{action: "Get_GalleryDetail_List"}:((cat=='GetVideoDetailList')?{action: "Get_VideoDetail_List"}:((cat=='GetVideoList')?{action: "Get_Video_List"}:((cat=='GetJobsList')?{action: "Get_Jobs_List"}:((cat=='GetFacultyList')?{action: "Get_Faculty_List"}:((cat=='GetAddmissionList')?{action: "Get_Addmission_List"}:((cat=='GetCarrerList')?{action: "Get_Carrer_List"}:((cat=='GetRegList')?{action: "Get_Reg_List"}:((cat=='GetChairmanList')?{action: "Get_Chairman_List"}:((cat=='GetNewssList')?{action: "Get_Newss_List"}:((cat=='GetPopupList')?{action: "Get_Popup_List"}:((cat=='GetNoticeList')?{action: "Get_Notice_List"}:((cat=='GetCalenderList')?{action: "Get_Calender_List"}:((cat=='GetDownloadList')?{action: "Get_Download_List"}:((cat=='GetHotnewsList')?{action: "Get_Hotnews_List"}:((cat=='GetDownloadprosList')?{action: "Get_Downloadpros_List"}:{}))))))))))))))))))))))))))))));
            }
            //Hide And Show Table Row Action
            function showHideAct(obj){
            $('.table').find('tr td.action>div.hideact').hide('slow');
            $('.table').find('tr td.action>a.showact').show('slow');
            $(obj).hide('slow');
            $(obj).next('div.hideact').show('slow');
            return false;
            }
            function setActiveClass(){
            var uriArray=returnUriArray();   
            var index=uriArray.length-1;
            var lastVal=uriArray[index];
            $urlcat='';
            if($.inArray("category", uriArray ) > -1){
            $urlcat="category";
            }
            else if($.inArray("home", uriArray ) > -1){
            $urlcat="home";
            }
            if($urlcat!=''){
            $('ul.nav').find('li').removeClass('active');
            $('ul.nav').find('li a').removeClass('active');
            if($('ul.lt').length>0)
            $('ul.lt').css('display:none');
            $('ul.nav').find('li').each(function(){
            $v=$(this).attr('data-attr');
            if($v==$urlcat){
            $(this).addClass('active');
            $(this).find('a').first().addClass('active');
            if($(this).find('ul.lt').length>0)
            $(this).find('ul.lt').css('display:block');
            return false;
            }
            });
            }
            }
            /********************************* Print page ********************/
            function printDiv(obj)
            {
            $(obj).hide();
            var divToPrint=document.getElementById('content');
            var newWin=window.open('','Print-Window');
            newWin.document.open();
            newWin.document.write('<html><head><link rel="stylesheet" href='+DASHSTATIC+'/admin/css/bootstrap.css"></head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
            newWin.document.close();
            $(obj).show();
            /*setTimeout(function(){newWin.close();$(obj).show();},10);*/
            }