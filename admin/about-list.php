<?php
session_start();

if($_SESSION["adminId"]!=1)
header('Location: index.php');
?>
<?php include('include/navbar.php'); ?>
<!-- partial -->
<?php include('include/sidebar.php'); ?>

<!-- partial -->
<div class="content-wrapper">
  <div class="container">
    <div class="row flex-grow">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title" style="float: left;">Generation List</h4>
            <h4 class="card-title" style="float: right;">
              <a href="<?php echo SITE_URL ?>admin/add-about.php">
                <button type="button" class="btn btn-info btn-fw">
                  <i class="fa fa-plus-square"></i>Add New
                </button>
              </a>
            </h4>
            <div class="table-responsive">
              <table class="table table-bordered" id="tableDataTable">
                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Image1</th>
                    <th>Image2</th>
                    <th>Status</th>
                    <th>Action</th>
                    
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/script.php'); ?>