
<?php require('../inc/config.php')?>

<!DOCTYPE html>

<html lang="en">

<head>

  <!-- Required meta tags -->

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Transport Admin Dashboard Panel</title>

  <!-- plugins:css -->

  <!-- <script src="js/bootstrap.js"></script>

  <script src="js/jquery.min.js"></script>-->

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

   <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 



  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">

  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">

  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">

  <!-- endinject -->

  <!-- plugin css for this page -->

  <!-- End plugin css for this page -->

  <!-- inject:css -->

  <link rel="stylesheet" href="css/style.css">

  <!-- endinject -->

  <link rel="shortcut icon" href="images/favicon.png" />

</head>

<script>

  var SITE_URL='<?php echo SITE_URL ?>';

</script>

<body>

  <div class="container-scroller">

    <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">

      <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">

        <div class="row w-100">

          <div class="col-lg-4 mx-auto">

            <div class="auto-form-wrapper">

              <form method="POST" action="" onsubmit="travellerAdminLogin(this,event,'.loginValidation');">

                <div class="form-group">

                  <label class="label">Username</label>

                  <div class="input-group">

                    <input type="text" class="form-control" name="email" placeholder="Username" required>

                    <div class="input-group-append">

                      <span class="input-group-text">

                        <i class="mdi mdi-check-circle-outline"></i>

                      </span>

                    </div>

                  </div>

                </div>

                <div class="form-group">

                  <label class="label">Password</label>

                  <div class="input-group">

                    <input type="password" class="form-control" name="password" placeholder="*********" required autocomplete="on">

                    <div class="input-group-append">

                      <span class="input-group-text">

                        <i class="mdi mdi-check-circle-outline"></i>

                      </span>

                    </div>

                  </div>

                </div>

                <input type="hidden" name="action" value="admin_login">

                <div class="form-group">

                  <button type="button" class="btn btn-primary submit-btn btn-block loginValidation">Login</button>

                </div>

                <div class="form-group d-flex justify-content-between">

                  <div class="form-check form-check-flat mt-0">

                    <label class="form-check-label">

                      <input type="checkbox" class="form-check-input" checked> Keep me signed in

                    </label>

                  </div>

                </div>

                

              </form>

            </div>

            <ul class="auth-footer">

              <li>

                <a href="#">Conditions</a>

              </li>

              <li>

                <a href="#">Help</a>

              </li>

              <li>

                <a href="#">Terms</a>

              </li>

            </ul>

            <p class="footer-text text-center">copyright © <b id="year"></b><script>var data=new Date();var year=data.getFullYear();document.getElementById('year').innerHTML=year;</script>&nbsp;Transport PVT. LTD. All rights reserved.</p>

          </div>

        </div>

      </div>

      <!-- content-wrapper ends -->

    </div>

    <!-- page-body-wrapper ends -->

  </div>

  <!-- container-scroller -->

  <!-- plugins:js -->

 <!-- plugins:js -->

  <script src="vendors/js/vendor.bundle.base.js"></script>

  <script src="vendors/js/vendor.bundle.addons.js"></script>

 

  <script src="js/off-canvas.js"></script>

  <script src="js/misc.js"></script>

  <!-- Custom js for this page-->

  <script src="js/dashboard.js"></script>

  <script src="js/validate.js"></script>

  <script src="js/admin.js"></script>

  <!-- End custom js for this page-->



<script src="js/bootstrap-flash-alert.js"></script>

</body>



</html>

<script type="text/javascript">



  var _gaq = _gaq || [];

  _gaq.push(['_setAccount', 'UA-36251023-1']);

  _gaq.push(['_setDomainName', 'jqueryscript.net']);

  _gaq.push(['_trackPageview']);



  (function() {

    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

  })();



</script>

  <style>

  	label.error{

  		display: none;

  	}

  </style>