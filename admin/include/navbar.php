<?php require('../inc/config.php')?>

<?php if(!isset($_SESSION['adminId']) && empty($_SESSION['adminId'])){

  header('location:'.SITE_URL.'admin/index.php');

}



?>

<!DOCTYPE html>

<html lang="en">

<head>

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Transport Admin Dashboard Panel</title>

  <!-- plugins:css -->

 <!-- <script src="js/bootstrap.js"></script>

  <script src="js/jquery.min.js"></script>-->

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

  <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> 

  <link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">

  <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">

  <link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">

  <link rel="stylesheet" href="vendors/iconfonts/font-awesome/css/font-awesome.css">

  <!-- inject:css -->

  <link rel="stylesheet" href="css/style.css">

  <link rel="stylesheet" href="js/summernote/summernote.css">

  <!-- endinject -->

  <link rel="shortcut icon" href="images/favicon.png" />

</head>



<body>

  <div class="container-scroller">

  <!-- partial:partials/_navbar.html -->

    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">

          <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">

            <a class="navbar-brand brand-logo" href="<?php echo SITE_URL ?>admin/dashboard.php">

             <img src="../assets/img/logo.png" style="background-color:#fff;width: 88px;height: auto;margin-top:20px;" alt="logo">

            </a>

            <a class="navbar-brand brand-logo-mini" href="<?php echo SITE_URL ?>admin/dashboard.php">

              <img src="images/logo-mini.svg" alt="logo" />

            </a>

          </div>

          <div class="navbar-menu-wrapper d-flex align-items-center">

            <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">

              <li class="nav-item">

                <a href="#" class="nav-link">Schedule

                  <span class="badge badge-primary ml-1">New</span>

                </a>

              </li>

              <li class="nav-item active">

                <a href="#" class="nav-link">

                  <i class="mdi mdi-elevation-rise"></i>Reports</a>

              </li>

              <li class="nav-item">

                <a href="#" class="nav-link">

                  <i class="mdi mdi-bookmark-plus-outline"></i>Score</a>

              </li>

            </ul>

            <ul class="navbar-nav navbar-nav-right">

              <li class="nav-item dropdown">

                <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">

                  <i class="mdi mdi-file-document-box"></i>

                  <span class="count">7</span>

                </a>

                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">

                  <div class="dropdown-item">

                    <p class="mb-0 font-weight-normal float-left">You have 7 unread mails

                    </p>

                    <span class="badge badge-info badge-pill float-right">View all</span>

                  </div>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <img src="images/faces/face4.jpg" alt="image" class="profile-pic">

                    </div>

                    <div class="preview-item-content flex-grow">

                      <h6 class="preview-subject ellipsis font-weight-medium text-dark">David Grey

                        <span class="float-right font-weight-light small-text">1 Minutes ago</span>

                      </h6>

                      <p class="font-weight-light small-text">

                        The meeting is cancelled

                      </p>

                    </div>

                  </a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <img src="images/faces/face2.jpg" alt="image" class="profile-pic">

                    </div>

                    <div class="preview-item-content flex-grow">

                      <h6 class="preview-subject ellipsis font-weight-medium text-dark">Tim Cook

                        <span class="float-right font-weight-light small-text">15 Minutes ago</span>

                      </h6>

                      <p class="font-weight-light small-text">

                        New product launch

                      </p>

                    </div>

                  </a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <img src="images/faces/face3.jpg" alt="image" class="profile-pic">

                    </div>

                    <div class="preview-item-content flex-grow">

                      <h6 class="preview-subject ellipsis font-weight-medium text-dark"> Johnson

                        <span class="float-right font-weight-light small-text">18 Minutes ago</span>

                      </h6>

                      <p class="font-weight-light small-text">

                        Upcoming board meeting

                      </p>

                    </div>

                  </a>

                </div>

              </li>

              <li class="nav-item dropdown">

                <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">

                  <i class="mdi mdi-bell"></i>

                  <span class="count">4</span>

                </a>

                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">

                  <a class="dropdown-item">

                    <p class="mb-0 font-weight-normal float-left">You have 4 new notifications

                    </p>

                    <span class="badge badge-pill badge-warning float-right">View all</span>

                  </a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <div class="preview-icon bg-success">

                        <i class="mdi mdi-alert-circle-outline mx-0"></i>

                      </div>

                    </div>

                    <div class="preview-item-content">

                      <h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>

                      <p class="font-weight-light small-text">

                        Just now

                      </p>

                    </div>

                  </a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <div class="preview-icon bg-warning">

                        <i class="mdi mdi-comment-text-outline mx-0"></i>

                      </div>

                    </div>

                    <div class="preview-item-content">

                      <h6 class="preview-subject font-weight-medium text-dark">Settings</h6>

                      <p class="font-weight-light small-text">

                        Private message

                      </p>

                    </div>

                  </a>

                  <div class="dropdown-divider"></div>

                  <a class="dropdown-item preview-item">

                    <div class="preview-thumbnail">

                      <div class="preview-icon bg-info">

                        <i class="mdi mdi-email-outline mx-0"></i>

                      </div>

                    </div>

                    <div class="preview-item-content">

                      <h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>

                      <p class="font-weight-light small-text">

                        2 days ago

                      </p>

                    </div>

                  </a>

                </div>

              </li>

              <li class="nav-item dropdown d-none d-xl-inline-block">

               

                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">

                  <a class="dropdown-item p-0">

                    <div class="d-flex border-bottom">

                      <div class="py-3 px-4 d-flex align-items-center justify-content-center">

                        <i class="mdi mdi-bookmark-plus-outline mr-0 text-gray"></i>

                      </div>

                      <div class="py-3 px-4 d-flex align-items-center justify-content-center border-left border-right">

                        <i class="mdi mdi-account-outline mr-0 text-gray"></i>

                      </div>

                      <div class="py-3 px-4 d-flex align-items-center justify-content-center">

                        <i class="mdi mdi-alarm-check mr-0 text-gray"></i>

                      </div>

                    </div>

                  </a>

                  <a class="dropdown-item mt-2">

                    Manage Accounts

                  </a>

                  <a class="dropdown-item">

                    Change Password

                  </a>

                  <a class="dropdown-item">

                    Check Inbox

                  </a>

                  <a href="<?php echo SITE_URL.'admin/logout.php'?>" class="dropdown-item">

                    Sign Out

                  </a>

                </div>

              </li>
                <li> <a href="logout.php" class="btn btn-info btn-lg">
                <span class="glyphicon glyphicon-log-out"></span> Log out
              </a> </li>
            </ul>

            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">

              <span class="mdi mdi-menu"></span>

            </button>

          </div>

        </nav>

        <style>

          .navbar.default-layout .navbar-brand-wrapper .navbar-brand img {

            width: calc(255px - 130px);

            max-width: 100%;

            height: 65px;

            margin: auto;

            vertical-align: middle;

            background-color: black;

        }

        </style>