<div class="container-fluid page-body-wrapper">
  <!-- partial:partials/_sidebar.html -->
  <nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-profile">
        <div class="nav-link">
          <div class="user-wrapper">
            <div class="profile-image">
             <!--  <img src="images/faces/face1.jpg" alt="profile image"> -->
            </div>
           <!--  <div class="text-wrapper">
              <p class="profile-name">Richard V.Welsh</p>
              <div>
                <small class="designation text-muted">Manager</small>
                <span class="status-indicator online"></span>
              </div>
            </div> -->
          </div>
        
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo SITE_URL ?>admin/dashboard.php">
          <i class="menu-icon mdi mdi-television"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#about" aria-expanded="false" aria-controls="gallery">
          <i class="menu-icon fa fa-credit-card"></i>
          <span class="menu-title">Manage About</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="about">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/add-about.php">Add About Detail</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/about-list.php">About Detail List</a>
            </li>
          </ul>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#service" aria-expanded="false" aria-controls="gallery">
          <i class="menu-icon fa fa-credit-card"></i>
          <span class="menu-title">Manage Service</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="service">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/add-service.php">Add Service Detail</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/service-list.php">Service Detail List</a>
            </li>
          </ul>
        </div>
      </li>


      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#testimonial" aria-expanded="false" aria-controls="testimonial">
          <i class="menu-icon fa fa-credit-card"></i>
          <span class="menu-title">Manage Testimonial</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="testimonial">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/add-testimonial.php">Add Testimonial</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/testimonial-list.php">Testimonial List</a>
            </li>
          </ul>
        </div>
      </li>   
    
 		<li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#contact" aria-expanded="false" aria-controls="ui-basic">
          <i class="menu-icon mdi mdi-content-copy"></i>
          <span class="menu-title">Contact Information</a></span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="contact">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="<?php echo SITE_URL ?>admin/contact-list.php">Contact List</a>
            </li>
          </ul>
        </div>
      </li>    
    </ul>
  </nav>

  <div class="main-panel"><!--  Pannel Start Here Only It has been closed in Footer.php page -->