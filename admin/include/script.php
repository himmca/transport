<!-- plugins:js -->
<script>
  var SITE_URL='<?php echo SITE_URL ?>';
</script>
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
 
  <script src="js/off-canvas.js"></script>
  <script src="js/misc.js"></script>
  <!-- Custom js for this page-->
  <script src="js/summernote/summernote.js"></script>
  <script src="js/dashboard.js"></script>
  <script src="js/validate.js"></script>
  <script src="js/admin.js"></script>
  <!-- End custom js for this page-->
<script src="js/bootstrap-flash-alert.js"></script>

</body>

</html>