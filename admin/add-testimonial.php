<?php
session_start();

if($_SESSION["adminId"]!=1)
header('Location: index.php');
?>
<?php include('include/navbar.php'); ?>
<!-- partial -->
<?php include('include/sidebar.php'); ?>
<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])){
$id=$_REQUEST['id'];
$editData=$CommonClass->ResultWithSingleRow("SELECT * FROM testimonial WHERE id=".$id."");
}
?>
<!-- partial -->
<div class="content-wrapper">
  <div class="row">
    <div class="row flex-grow">
      <div class="col-12">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">Add Testimonial Details</h4>
            <form class="forms-sample formcontrol-area" role="form" enctype="multipart/form-data"  onsubmit="AddTestimonialDetails(this,event,'.form_validation');">
             <div class="col-12 form-group">
                <label for="name">Message</label>
                  <textarea class="form-control summernote" name="message" id="message" placeholder="Description"  required tabindex="3" rows="10"><?=(!empty($editData['message']))?$editData['message']:''; ?> </textarea>
              </div>
               <div class="col-12 form-group">
                <label for="name">Designation</label>
                <input type="text" class="form-control" name="designation" id="designation" value="<?php echo isset($editData['designation'])?$editData['designation']:''?>" placeholder="Designation" tabindex="1">
              </div>
              <div class="col-12 form-group">
                <label for="name">Parent Name</label>
                <input type="text" class="form-control" name="name" id="name" value="<?php echo isset($editData['name'])?$editData['name']:''?>" placeholder="Client Name" tabindex="1">
              </div>
              
              <div style="clear: both;"></div>
              
              <div class="col-12 form-group" title="Upload facility icon image">
                <input type="file" name="image" id="image" tabindex="4" onchange="fileuploadpreview(this);" class="file-upload-browse btn btn-info" <?php echo !empty($editData['image'])?'':'required' ?>>
                <div class="img-responsive previewimg">
                  <?=( !empty($editData['image']) )? '<img width="100px" src="'.SITE_URL.'admin/uploads/testimonial/'.$editData['image'].'">':'' ;?>
                </div>
              </div>
              <div style="clear: both;"></div>
              <input type="hidden" name="action" value="add_testimonial_data">
              <input type="hidden" name="hiddenval" id="hiddenval" value="<?=!empty($editData['id'])?$editData['id']:"";?>" >
              <div class="col-6 form-group">
                <button type="button" class="btn btn-success mr-2 form_validation" tabindex="2">Submit</button>
                <a href="<?php echo SITE_URL ?>admin/testimonial-list.php" tabindex="3"><button type="button" class="btn btn-light">Cancel</button></a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <?php include('include/footer.php'); ?>
  <?php include('include/script.php'); ?>
  <script type="text/javascript">
$('.summernote').summernote({
height: 120,   //set editable area's height
});
</script>