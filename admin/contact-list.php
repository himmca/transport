<?php
session_start();

if($_SESSION["adminId"]!=1)
header('Location: index.php');
?>
 <?php include('include/navbar.php'); ?>
 <!-- partial -->
 <?php include('include/sidebar.php'); ?>
 <!-- partial -->
 <div class="content-wrapper">
  <div class="contanier">

    <div class="row flex-grow">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title" style="float: left;">Contact List</h4>
            <h4 class="card-title" style="float: right;">
              </a>
            </h4>
            <div class="table-responsive">
              <table class="table table-bordered" id="tableDataTable">
                <thead>
                  <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Parent Name</th>
                    <th>Class</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Message</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('include/footer.php'); ?>
<?php include('include/script.php'); ?>