<?php
include '../inc/config.php';
	$action=$_POST['action'];
$data=$CommonClass->sanitize($_POST);
switch ($action) {

	case 'Change_Status':
		$return['valid'] = $AdminClass->change_status($data);
		break;

	case 'Delete_Record':
		$return['valid'] = $AdminClass->delete_record($data);
		break;
	
	case 'admin_login':
		$result=$AdminClass->adminLoginCheck($data);
		if($result==1){
		    session_start();
			$_SESSION['adminId']=1;
			$return['valid']=true;
		}
		else{
			unset($_SESSION['adminId']);
			$return['valid']=false;
		}
		break;
	
	case 'add_slider':
		if($data['hiddenval']>0)
			$count=$CommonClass->CountRecords("SELECT count(*) as nums FROM sliders WHERE order_no=".$data['order']." AND id!=".$data['hiddenval']." ");
		else
			$count=$CommonClass->CountRecords("SELECT count(*) as nums FROM sliders WHERE order_no=".$data['order']."");
			if ($count>0)
			$return= 2;
			else{
				if(isset($_FILES['sliderImg']['name']) && !empty($_FILES['sliderImg']['name'])){
					if(!empty($_FILES['sliderImg']['tmp_name'])){
					$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['sliderImg']['name']);
					move_uploaded_file($_FILES['sliderImg']['tmp_name'], "uploads/slider/".$uploadpic);
					$data['image']=$uploadpic;
					}				
				}
				$return = $AdminClass->addSlider($data);
			}
		break;

	case 'Get_Slider_List':
			$return['id']=$AdminClass->GetSliderList();
			$return['valid']=true;
		break;
		
		case 'add_about_data':
		if($data['hiddenval']>0)
		$count=$CommonClass->CountRecords("SELECT count(*) as nums FROM about WHERE description=".$data['description']." AND id!=".$data['hiddenval']." ");
		else
		$count=$CommonClass->CountRecords("SELECT count(*) as nums FROM about WHERE description=".$data['description']."");
		if ($count>0)
		$return= 2;
		else{
		if(isset($_FILES['image1']['name']) && !empty($_FILES['image1']['name'])){
			if(!empty($_FILES['image1']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image1']['name']);
				move_uploaded_file($_FILES['image1']['tmp_name'], "uploads/about/".$uploadpic);
				$data['image1']=$uploadpic;
				
			}				
		}
		if(isset($_FILES['image2']['name']) && !empty($_FILES['image2']['name'])){
			if(!empty($_FILES['image2']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image2']['name']);
				move_uploaded_file($_FILES['image2']['tmp_name'], "uploads/about/".$uploadpic);
				$data['image2']=$uploadpic;
				
			}				
		}
		$return = $AdminClass->addAboutData($data);
	}
	break;
		
	case 'Get_About_List':
			$return['id']=$AdminClass->GetAboutList();
			$return['valid']=true;
		break;

	case 'add_gallery_data':

		$return = $AdminClass->addGalleryData($data);
				
	break; 	

	case 'Get_Gallery_List':
			$return['id']=$AdminClass->GetGalleryList();
			$return['valid']=true;
		break;

		case 'add_popup_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/popup/".$uploadpic);
				$data['image']=$uploadpic;
				
			}				
		}
				$return = $AdminClass->addPopupData($data);
		break; 

	case 'Get_Popup_List': 
			$return['id']=$AdminClass->GetPopupList();
			$return['valid']=true;
		break;
	
		case 'add_principal_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/principal/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addPrincipalData($data);
		break; 

	case 'Get_Principal_List':
			$return['id']=$AdminClass->GetPrincipalList();
			$return['valid']=true;
		break;

	case 'add_director_data':
		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
				if(!empty($_FILES['image']['tmp_name'])){
					$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
					move_uploaded_file($_FILES['image']['tmp_name'], "uploads/director/".$uploadpic);
					$data['image']=$uploadpic;
				
				}				
			}
					$return = $AdminClass->addDirectorData($data);
	break; 
	
	case 'Get_Director_List':
				$return['id']=$AdminClass->GetDirectorList();
				$return['valid']=true;
	break;
	case 'add_chairman_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/chairman/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addChairmanData($data);
		break; 

	case 'Get_Chairman_List':
			$return['id']=$AdminClass->GetChairmanList();
			$return['valid']=true;
			break;
	case 'add_newss_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/newss/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addNewssData($data);
		break; 

	case 'Get_Newss_List':
			$return['id']=$AdminClass->GetNewssList();
			$return['valid']=true;
	break;
	case 'add_calender_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/calender/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addCalenderData($data);
		break; 

	case 'Get_Calender_List':
			$return['id']=$AdminClass->GetCalenderList();
			$return['valid']=true;
		break;
			
	case 'add_download_data':
	if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			if(!empty($_FILES['file']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], "uploads/download/".$uploadpic);
				$data['file']=$uploadpic;
			}				
		} 
		
		$return = $AdminClass->addDownloadData($data);
		break; 

	case 'Get_Download_List':
			$return['id']=$AdminClass->GetDownloadList();
			$return['valid']=true;
		break; 
		
	case 'add_downloadpros_data':
	if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			if(!empty($_FILES['file']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], "uploads/download/".$uploadpic);
				$data['file']=$uploadpic;
			}				
		} 
				$return = $AdminClass->addDownloadprosData($data);
		break; 

	case 'Get_Downloadpros_List':
			$return['id']=$AdminClass->GetDownloadprosList();
			$return['valid']=true;
	break;
		case 'add_news_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/news/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addNewsData($data);
		break; 

	case 'Get_News_List':
			$return['id']=$AdminClass->GetNewsList();
			$return['valid']=true;
		break;
	case 'add_coverage_data':
		if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/coverage/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addCoverageData($data);
		break; 

	case 'Get_Coverage_List':
			$return['id']=$AdminClass->GetCoverageList();
			$return['valid']=true;
		break;


		case 'add_generation_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/generation/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addGenerationData($data);
		break; 

	case 'Get_Generation_List':
			$return['id']=$AdminClass->GetGenerationList();
			$return['valid']=true;
		break;
				case 'add_game_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/generation/".$uploadpic);
				$data['image']=$uploadpic;
			
			}				
		}
				$return = $AdminClass->addGameData($data);
		break; 

	case 'Get_Game_List':
			$return['id']=$AdminClass->GetGameList();
			$return['valid']=true;
		break;

	case 'add_testimonial_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/testimonial/".$uploadpic);
				$data['image']=$uploadpic;
			}				
		}
		$return = $AdminClass->addTestimonialData($data);
	break; 
		
	case 'Get_Testimonial_List':
			$return['id']=$AdminClass->GetTestimonialList();
			$return['valid']=true;
	break;
	
	case 'add_faculty_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/faculty/".$uploadpic);
				$data['image']=$uploadpic;
			}				
		}
		$return = $AdminClass->addFacultyData($data);
	break; 
		
	case 'Get_Faculty_List':
			$return['id']=$AdminClass->GetFacultyList();
			$return['valid']=true;
	break;



		case 'add_media_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/testimonial/".$uploadpic);
				$data['image']=$uploadpic;
			}				
		}
		$return = $AdminClass->addMediaData($data);
		break; 
		
	case 'Get_Media_List':
			$return['id']=$AdminClass->GetMediaList();
			$return['valid']=true;
		break;
	case 'add_hotnews_data':
		$return = $AdminClass->addHotnewsData($data);
	break; 
		
	case 'Get_Hotnews_List':
			$return['id']=$AdminClass->GetHotnewsList();
			$return['valid']=true;
	break;
		
	case 'add_notice_data':
		$return = $AdminClass->addNoticeData($data);
	break; 
		
	case 'Get_Notice_List':
			$return['id']=$AdminClass->GetNoticeList();
			$return['valid']=true;
	break;

	case 'add_job_data':
		$return = $AdminClass->addJobData($data);
	break; 
		
	case 'Get_Jobs_List':
			$return['id']=$AdminClass->GetJobsList();
			$return['valid']=true;
	break;
	case 'add_achievers_data':
	if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
			if(!empty($_FILES['image']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'], "uploads/achievers/".$uploadpic);
				$data['image']=$uploadpic;
			}				
		}
		$return = $AdminClass->addAchieversData($data);
		break; 
		
	case 'Get_Achievers_List':
			$return['id']=$AdminClass->GetAchieversList();
			$return['valid']=true;
		break;


case 'add_tc_data':
if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			if(!empty($_FILES['file']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], "uploads/tc/".$uploadpic);
				$data['file']=$uploadpic;
			}				
		}
		$return = $AdminClass->addTcData($data);
		break; 
		
	case 'Get_Tc_List':
			$return['id']=$AdminClass->GetTcList();
			$return['valid']=true;
		break;

		case 'add_reg_data':
		if(isset($_FILES['file']['name']) && !empty($_FILES['file']['name'])){
			if(!empty($_FILES['file']['tmp_name'])){
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['file']['name']);
				move_uploaded_file($_FILES['file']['tmp_name'], "uploads/reg/".$uploadpic);
				$data['file']=$uploadpic;
			}				
		}
		$return = $AdminClass->addRegData($data);
		break; 
		
	case 'Get_Reg_List':
			$return['id']=$AdminClass->GetRegList();
			$return['valid']=true;
		break;


		
		
	case 'Get_Addmission_List':
			$return['id']=$AdminClass->GetAddmissionList();
			$return['valid']=true;
		break;
	case 'Get_Carrer_List':
			$return['id']=$AdminClass->GetCarrerList();
			$return['valid']=true;
		break;
		
	case 'add_gallerydetail_data':
		$return = $AdminClass->addGalleryDetailData($data);
		break; 
		
	case 'Get_GalleryDetail_List':
			$return['id']=$AdminClass->GetGalleryDetailList();
			$return['valid']=true;
		break;
	
	case 'add_videodetail_data':
		$return = $AdminClass->addVideoDetailData($data);
		break; 
		
	case 'Get_VideoDetail_List':
			$return['id']=$AdminClass->GetVideoDetailList();
			$return['valid']=true;
		break;
	case 'add_video_data':
		$return = $AdminClass->addVideoData($data);
		break; 
		
	case 'Get_Video_List':
			$return['id']=$AdminClass->GetVideoList();
			$return['valid']=true;
		break;


	case 'add_Package_Image':
		if(isset($_FILES['packageImg']['name']) && !empty($_FILES['packageImg']['name'])){
			foreach ($_FILES['packageImg']['name'] as $k => $value) {
					$uploadpic=str_replace(' ', '',$_FILES['packageImg']['name'][$k]);
					move_uploaded_file($_FILES['packageImg']['tmp_name'][$k], "uploads/package/".$uploadpic);
					$result=$CommonClass->NormalQuery("INSERT INTO `package_images`(`package_id`, `image`) VALUES ('".$data['packageId']."','".$uploadpic."')");
			}
		$return = $result;
		}
		break; 
		case 'Get_Volunteer_List':
			$return['id']=$AdminClass->GetVolunteerList();
			$return['valid']=true;
		break;
		case 'Get_Participants_List':
			$return['id']=$AdminClass->GetParticipantsList();
			$return['valid']=true;
		break;
		case 'Get_Contact_List':
			$return['id']=$AdminClass->GetContactList();
			$return['valid']=true;
		break;

		case 'update_Gallery_Images':

			if(isset($_FILES['image']['name']) && !empty($_FILES['image']['name'])){
				$countfiles = count($_FILES['image']['name']);
				move_uploaded_file($_FILES['image']['tmp_name'][$i],'uploads/gallery/'.$uploadpic);
				
				$uploadpic=date('Y_m_d_h_i_s').'_'.str_replace(' ', '',$_FILES['image']['name'][$i]);
				
				$temporary = '';
				
				// $temp = ("SELECT * from `gallery_images` WHERE `status`=1");
				
				// for($i=0;$i<$countfiles;$i++){
			
				
					
				// $temp = $temp . $uploadpic . '#';
				
				// }
				// $data['image']=$temporary;
				} 
			$return['id'] = $AdminClass->updateGalleryImages();
			$return['valid'] = true;
			break;
		
		
	default:		
		break;
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true ");
header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
echo json_encode($return);


?>